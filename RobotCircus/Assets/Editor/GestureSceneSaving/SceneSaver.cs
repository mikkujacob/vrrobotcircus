﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using System.Linq;
using System;
using System.IO;


public class SceneSaver : MonoBehaviour {

	/// <summary>
	/// The resources folder location. Please edit for each instance.
	/// </summary>
	public string resourcesFolderLocation = "C:\\Users\\mjacob6\\Documents\\code-repositories\\MImE\\VirtualPlaymat\\Assets\\Resources";
	public string fileName = "SceneSaverTextFile";
	public string textFilePath;
	public bool clickToRun = false;
	public GameObject[] allObjects;


	public void writeToFile(string desiredFileName, string desiredFilePath)
	{
		List<string> placeHolderList = new List<string>();
		List<string> finalList = new List<string>();
		int indexCounter = 0;

		allObjects = (GameObject[]) GameObject.FindObjectsOfType(typeof(GameObject));
		foreach (GameObject someObject in allObjects)
		{
			if (someObject.activeInHierarchy && someObject.transform.parent == null)
			{

				Vector3 somePosition = someObject.transform.position;
				float positionX  = somePosition.x;
				float positionY  = somePosition.y;
				float positionZ  = somePosition.z;
				string positionString = positionX.ToString("R") + "," + positionY.ToString("R") + "," + positionZ.ToString("R");


				Quaternion someRotation = someObject.transform.rotation;
				float rotationX = someRotation.x;
				float rotationY = someRotation.y;
				float rotationZ = someRotation.z;
				float rotationW = someRotation.w;
				string rotationString = rotationX.ToString() + "," + rotationY.ToString() + "," + rotationZ.ToString() + "," + rotationW.ToString();

				Vector3 someScale = someObject.transform.localScale;
				float scaleX  = someScale.x;
				float scaleY  = someScale.y;
				float scaleZ  = someScale.z;
				string scaleString = scaleX.ToString("R") + "," + scaleY.ToString("R") + "," + scaleZ.ToString("R");

				string pathString = AssetDatabase.GetAssetPath(PrefabUtility.GetPrefabParent(someObject));


				if (!pathString.Contains(".prefab") || !pathString.Contains("/Resources/"))
				{
					Debug.Log("[Warning]: object " + someObject.name + " does not have a correctly created prefab, A prefab will be created under Resources");
					/*if (!Directory.Exists(resourcesFolderLocation + "\\" + desiredFileName + " Assets (scene: " + SceneManager.GetActiveScene().name + ")"))
					{
						Debug.Log(AssetDatabase.CreateFolder("Assets/Resources",  desiredFileName + " Assets (scene: " + SceneManager.GetActiveScene().name +")"));
					}*/



					string newPrefabPath = "Assets/Resources/" + desiredFileName + " Assets (scene " + SceneManager.GetActiveScene().name +")/" + someObject.name + ".prefab";
					string systemPrefabPath = resourcesFolderLocation + "\\" + desiredFileName + " Assets (scene " + SceneManager.GetActiveScene().name +")\\" + someObject.name + ".prefab";
					Directory.CreateDirectory(Path.GetDirectoryName(systemPrefabPath));

					PrefabUtility.CreatePrefab(newPrefabPath, someObject, ReplacePrefabOptions.ConnectToPrefab);
					pathString = AssetDatabase.GetAssetPath(PrefabUtility.GetPrefabParent(someObject));
				}


				string[] splitter = {"/Resources/", ".prefab"};
				Debug.Log(pathString);
				string[] segments = pathString.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
				/*for (int i = 0; i < segments.Length; i++)
				{
					Debug.Log(i + " " + segments[i]);
				}*/
				//Debug.Log(AssetDatabase.GetAssetPath(PrefabUtility.GetPrefabParent(someObject)));
				pathString = segments[1];
				string trackingType = "None";
				if (someObject.tag == "ObjectStateTracker")
				{
					trackingType = "Object";
					//UnityEngine.Object.DestroyImmediate(((GameObject)Resources.Load(pathString)).GetComponent<ObjectStateTracker>(), true);
				}
				if (someObject.tag == "KinectStateTracker")
				{
					trackingType = "Kinect";
				}



				string newLine = indexCounter + "," + someObject.name + "," + positionString + "," + rotationString + "," + scaleString + "," + pathString + "," + trackingType;

				if (someObject.tag == "NVR Player" || someObject.tag == "MImEGlobalObject")
				{
					finalList.Add(newLine);
				}
				else
				{
					placeHolderList.Add(newLine);
				}

				indexCounter++;

			}
		}


		string filePath = resourcesFolderLocation + "\\" + desiredFileName + ".txt";
		if (desiredFilePath != null && desiredFilePath != "")
		{
			filePath  = desiredFilePath + "\\" + desiredFileName + ".txt";
			//Debug.Log(filePath);
		}
		Directory.CreateDirectory(Path.GetDirectoryName(filePath));
		finalList.AddRange(placeHolderList);
		System.IO.File.WriteAllLines(@filePath, finalList.ToArray());
		Debug.Log(filePath);
	}


	void Update ()
	{
		if (clickToRun == true)
		{
			clickToRun = false;
			writeToFile(fileName, textFilePath);
			Debug.Log("I am not insane");

		}

	}


	enum TrackingType
	{
		Object,
		Kinect,
		None
	}

}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class DoorActivatorMecanim : MonoBehaviour 
{
    private Animator DoorAnimator;

	void Start()
	{
        Debug.Log("Awake");
        DoorAnimator = GetComponent<Animator> ();
	}

	void OnTriggerStay(Collider col)
	{
        Debug.Log("triggered");
        DoorAnimator.SetTrigger("open");
	}

    void OnTriggerEnter(Collider col)
    {
        Debug.Log("triggered");
        DoorAnimator.SetTrigger("open");
    }

    void OnTriggerExit(Collider col)
    {
        Debug.Log("triggered");
        DoorAnimator.SetTrigger("open");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelmanager : MonoBehaviour {

	public AudioSource HCapplause;
	public AudioSource SRapplause;
	public AudioSource SLapplause;
	private bool startClapping = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.A)) {
			startClapping = true;
		}

		if (startClapping) {
			HCapplause.Play ();
			SRapplause.Play ();
			SLapplause.Play ();
		}

		startClapping = false;

	}
}

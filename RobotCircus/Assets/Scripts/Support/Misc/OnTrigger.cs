using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Component that handles detection of trigger events
/// </summary>
public class OnTrigger : MonoBehaviour {

	/// <summary>
	/// Updates the tracker.
	/// </summary>
	private ObjectStateTracker tracker;

	/// <summary>
	/// Reference to the ContainmentTracker
	/// </summary>
	TriggerContainmentTracker triggerContainmentTracker;

	/// <summary>
	/// Mesh of the object
	/// </summary>
	private Mesh mesh;

	/// <summary>
	/// size of the object
	/// </summary>
	private Vector3 size;

	/// <summary>
	/// position of the object
	/// </summary>
	private Vector3 position;

	/// <summary>
	/// internal contains
	/// </summary>
	public List<string> contains;

	/// <summary>
	/// internal containing
	/// </summary>
	public bool containing;

	/// <summary>
	/// internal containers
	/// </summary>
	public List<string> containers;

	/// <summary>
	/// internal contained
	/// </summary>
	public bool contained;

	/// <summary>
	/// Makes sure everything waits untill all the initial values have been set
	/// </summary>
	private bool initialized = false;

	/// <summary>
	/// Initializes everything on OnTrigger's side
	/// </summary>
	public void Setup(List<string> contains, bool containing, List<string> containers, bool contained, TriggerContainmentTracker triggerContainmentTracker)
	{

		this.triggerContainmentTracker = triggerContainmentTracker;
		tracker = gameObject.GetComponent<ObjectStateTracker>();
		mesh = gameObject.GetComponent<MeshFilter>().mesh;
		size = Vector3.Scale(mesh.bounds.extents, gameObject.transform.lossyScale);
		position = gameObject.GetComponent<Renderer>().bounds.center;

		this.contains = new List<string>();
		this.containers = new List<string>();

		this.contains.AddRange(contains);
		this.containers.AddRange(containers);

		this.containing = containing;
		this.contained = contained;

		initialized = true;

	}

	/// <summary>
	/// returns true when a is bigger then b
	/// </summary>
	private bool IsFirstBiggerThenSecond(Vector3 a, Vector3 b)
	{
		if (a.x >= b.x && a.y >= b.y && a.z >= b.z)
		{
			return true;
		}
		return false;
	}


	/// <summary>
	/// returns true when a fulley encompasses b within it
	/// </summary>
	private bool DoesFirstEncompassSecond(Vector3 aExtents, Vector3 aPosition, Vector3 bExtents, Vector3 bPosition)
	{
		if (aPosition.x + aExtents.x >= bPosition.x && aPosition.x - aExtents.x <= bPosition.x)
		{
			if (aPosition.y + aExtents.y >= bPosition.y && aPosition.y - aExtents.y <= bPosition.y)
			{
				if (aPosition.z + aExtents.z >= bPosition.z && aPosition.z - aExtents.z <= bPosition.z)
				{
					return true;
				}
			}
		}
		return false;
	}

	/// <summary>
	/// runs for both colliders whenever two colliders interact with one another, gievn one of then having a trigger colliders and one having a rigid body
	/// </summary>
	void OnTriggerStay (Collider other)
	{

		if (other.gameObject.tag == "ObjectStateTracker" && initialized)
		{
			//Debug.Log("OnTriggerEnter happened on " + gameObject.name + " with " + other.gameObject.name);


			position = gameObject.GetComponent<Renderer>().bounds.center;

			ObjectStateTracker otherTracker = other.gameObject.GetComponent<ObjectStateTracker>();
			Mesh otherMesh = other.gameObject.GetComponent<MeshFilter>().mesh;
			Vector3 otherSize = Vector3.Scale(otherMesh.bounds.extents, other.gameObject.transform.lossyScale);
			Vector3 otherPosition = other.gameObject.GetComponent<Renderer>().bounds.center;
			//Debug.Log(gameObject.name + ": " + size + ". " + other.gameObject.name + ": " + otherSize + "------------");
			//Debug.Log(gameObject.name + ": " + position + ". " + other.gameObject.name + ": " + otherPosition + "++++++++++++");

			if (IsFirstBiggerThenSecond(size, otherSize))
			{
				// size is bigger, we are the container
				//Debug.Log(gameObject.name + " is the container");

				if (DoesFirstEncompassSecond(size, position, otherSize, otherPosition))
				{
					// we contain other entirely inside of us
					//Debug.Log(gameObject.name + " entirely encompasses " + other.gameObject.name);
					
					if (!contains.Contains(otherTracker.state.id))
					{
						containing = true;
						contains.Add(otherTracker.state.id);
					}
				}
				else
				{
					//we do NOT entirely contain other inside of us
					//Debug.Log(gameObject.name + " partially encompasses " + other.gameObject.name);

					if (contains.Contains(otherTracker.state.id))
					{
						contains.Remove(otherTracker.state.id);
						if (contains.Count == 0)
						{
							containing = false;
						}
					}
				}
			}
			else
			{
				//otherSize is bigger, we are the contained object
				//Debug.Log(gameObject.name + " is not the container");

				if (DoesFirstEncompassSecond(otherSize, otherPosition, size, position))
				{
					// we are entirely contained in other
					//Debug.Log(gameObject.name + " is entirely inside " + other.gameObject.name + " 33");

					if (!containers.Contains(otherTracker.state.id))
					{
						contained = true;
						containers.Add(otherTracker.state.id);
						tracker.isChanged = true;
					}
				}
				else
				{
					//we are partially contained in other
					//Debug.Log(gameObject.name + " is partially inside " + other.gameObject.name + " 33");

					if (containers.Contains(otherTracker.state.id))
					{

						containers.Remove(otherTracker.state.id);
						tracker.isChanged = true;
						if (containers.Count == 0)
						{
							contained = false;
						}
					}
				}
			}
		}
	}






}

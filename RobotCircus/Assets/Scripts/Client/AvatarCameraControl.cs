﻿using UnityEngine;
using System.Collections;

public class AvatarCameraControl : MonoBehaviour {

    GameObject Camera;
    Transform CameraPosition;
    Vector3 EyeOffset;

	// Use this for initialization
	void Start () {
        Camera = GameObject.FindGameObjectsWithTag("MainCamera")[0];
        CameraPosition = Camera.transform;
        EyeOffset = new Vector3(0, 1.626085f, 0.06178211f);
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.position = CameraPosition.position - EyeOffset;
//        Debug.Log("Camera Position is" + CameraPosition.position);
	}
}

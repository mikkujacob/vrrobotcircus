﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class HeadTransformVRUpdater : MonoBehaviour
{
	GameObject cameraObject;

	// Use this for initialization
	void Start()
	{
		cameraObject = GameObject.Find("Head");
	}

	// Update is called once per frame
	void Update()
	{
		gameObject.transform.position = cameraObject.transform.position - (Vector3.up * 2f);
		//gameObject.transform.rotation = cameraObject.transform.rotation;
	}
}

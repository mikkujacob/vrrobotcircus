﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class LockedBox : MonoBehaviour {

	private List<Screw> screws;
	private bool open = false;
	private List<GameObject> parts;
	// Use this for initialization
	void Start () {
		screws = new List<Screw>(gameObject.GetComponentsInChildren<Screw>());
		parts = new List<GameObject>();
		foreach (Transform someTransform in gameObject.transform) {
			if (someTransform.parent == gameObject.transform && someTransform.GetComponent<Screw>() == null && someTransform.GetComponent<NVRInteractableItem>() != null)
			{
				parts.Add(someTransform.gameObject);
				someTransform.GetComponent<NVRInteractableItem>().enabled = false;
				GameObject.Destroy(someTransform.gameObject.GetComponent<Rigidbody>());

			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (!open && !IsStillLocked())
		{
			openAndDie();
		}
	}

	private bool IsStillLocked ()
	{

		foreach (Screw screw in screws) {
			if (screw.screwedInPlace)
			{
				return true;
			}
		}
		return false;
	}


	private void openAndDie()
	{
		foreach (GameObject part in parts) {

			NVRInteractableItem nvrInteractableItem = part.GetComponent<NVRInteractableItem>();
			if (nvrInteractableItem != null)
			{
				nvrInteractableItem.Rigidbody = part.AddComponent<Rigidbody>();
				nvrInteractableItem.enabled = true;
			}
			part.transform.parent = null;
			nvrInteractableItem.UpdateColliders();
		}
		gameObject.GetComponent<NVRInteractableItem>().UpdateColliders();
		open = true;
		//GameObject.Destroy(gameObject);
	}
}

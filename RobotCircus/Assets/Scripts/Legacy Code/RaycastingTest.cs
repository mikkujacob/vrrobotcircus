﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class RaycastingTest : MonoBehaviour {

	private NVRPlayer player;
	private GameObject[] lines;
	private LineRenderer[] lasers;



	// Use this for initialization
	void Start () {
		player = this.GetComponent<NVRPlayer>();
		lines = new GameObject[player.Hands.Length];
		lasers = new LineRenderer[lines.Length];
		for (int i = 0; i < lines.Length; i++)
		{
			lines[i] = new GameObject("Dor's " + player.Hands[i].name + " test object");
			lasers[i] = lines[i].AddComponent<LineRenderer>();
			lasers[i].startColor = Color.magenta;
			lasers[i].endColor = Color.green;
			lasers[i].startWidth = 0.03f;
			lasers[i].endWidth = 0.02f;

		}
	}

	/*public override void UseButtonDown()
	{
		for (int i = 0; i < player.Hands.Length; i++){
			if (player.Hands[i].gameObject.activeInHierarchy == false || player.Hands[i].IsCurrentlyTracked == false)
			{
					continue;
			}
			lasers[i].startColor = Color.cyan;
		}
	}*/

	// Update is called once per frame
	void Update () {
		for (int i = 0; i < player.Hands.Length; i++){
			if (player.Hands[i].gameObject.activeInHierarchy == false || player.Hands[i].IsCurrentlyTracked == false)
			{
					continue;
			}
			RaycastHit hit;
			Vector3 origin = player.Hands[i].CurrentPosition;
			Vector3 endPoint = origin + player.Hands[i].CurrentForward;
			Ray landingRay = new Ray(origin, endPoint - origin);
			//lasers[i].SetPositions(new Vector3[] { origin, endPoint });
			//Debug.DrawRay(player.Hands[i].CurrentPosition, player.Hands[i].CurrentForward * 10.0f);
			//Debug.Log(landingRay);
			lines[i].SetActive(false);
			if (Physics.Raycast(landingRay, out hit, endPoint.magnitude))
			{
				lines[i].SetActive(true);
				lasers[i].SetPositions(new Vector3[] { origin, hit.point });
				lasers[i].startWidth = 0.03f;
				if(player.Hands[i].UseButtonPressed == true)
				{
					lasers[i].startWidth = 0.05f;
				}

			}







		}

	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Timer : MonoBehaviour
{
	public int timeLeft = 20;
	public Text countdownText;
	public int score = 0;
	public int totalScore = 0;
	public int numObjects = 0;
	private Dictionary<GameObject, string> gameObjectDictionary;
	private List<GameObject> gameObjectKeyList;
	bool scoreBoard = false;
	bool complete = false;

	// Use this for initialization
	void Start ()
	{
		//StartCoroutine("LoseTime");
		gameObjectDictionary = new Dictionary<GameObject, string> ();
		gameObjectKeyList = new List<GameObject> ();
		gameObjectKeyList.AddRange (GameObject.FindGameObjectsWithTag ("ObjectStateTracker"));
		foreach (GameObject gameObj in gameObjectKeyList)
		{
			gameObjectDictionary.Add (gameObj, "Instructions... " + gameObj.name);
		}

		Invoke ("LateStart", 2);
	}

	void LateStart()
	{
		foreach (GameObject gameObj in gameObjectKeyList)
		{
			gameObj.SetActive (false);
		}

		gameObjectKeyList [0].SetActive (true);
	}

	// Update is called once per frame
	void Update ()
	{

		if (Input.GetKeyDown ("down"))
		{
			StopCoroutine ("LoseTime");
			timeLeft = 20;
			numObjects = 0;
			score = 0;
			totalScore = 0;
			scoreBoard = false;
			complete = false;
			foreach (GameObject gameObject in gameObjectKeyList)
			{
				gameObject.SetActive (false);
			}

			gameObjectKeyList [0].SetActive (true);

			StartCoroutine ("LoseTime");
		}

		if (complete)
		{
			gameObjectKeyList [numObjects].SetActive (false);
			countdownText.text = ("Total Score: " + totalScore);
		}
		else
		{
			if (scoreBoard)
			{
				countdownText.text = ("Score: " + score + "\nTime Left = " + timeLeft);
			}
			else
			{

				countdownText.text = ("Instruction: " + gameObjectDictionary [gameObjectKeyList [numObjects]] + "\nTime Left = " + timeLeft);
			}
		}

		if ((timeLeft <= 0) && scoreBoard)
		{
			StopCoroutine ("LoseTime");
			gameObjectKeyList [numObjects].SetActive (false);
			numObjects = numObjects + 1;
			if (numObjects >= gameObjectKeyList.Count)
			{
				numObjects = numObjects - 1;
				complete = true;
			}
			gameObjectKeyList [numObjects].SetActive (true);
			timeLeft = 20;
			scoreBoard = false;
			score = 0;
			StartCoroutine ("LoseTime");
		}
		else if ((timeLeft <= 0) && !scoreBoard)
		{
			StopCoroutine ("LoseTime");
			timeLeft = 5;
			scoreBoard = true;
			StartCoroutine ("LoseTime");
		}

		if (Input.GetKeyDown ("right"))
		{
			StopCoroutine ("LoseTime");
			gameObjectKeyList [numObjects].SetActive (false);
			numObjects = numObjects + 1;
			if (numObjects >= gameObjectKeyList.Count)
			{
				numObjects = 0;
			}
			gameObjectKeyList [numObjects].SetActive (true);
			timeLeft = 20;
			scoreBoard = false;
			score = 0;
			StartCoroutine ("LoseTime");
		}

		if (Input.GetKeyDown ("left"))
		{
			StopCoroutine ("LoseTime");
			gameObjectKeyList [numObjects].SetActive (false);
			numObjects = numObjects - 1;
			if (numObjects < 0)
			{
				numObjects = gameObjectKeyList.Count - 1;
			}
			gameObjectKeyList [numObjects].SetActive (true);
			timeLeft = 20;
			scoreBoard = false;
			score = 0;
			StartCoroutine ("LoseTime");
		}

		if (Input.GetKeyDown ("1"))
		{
			score = 1;
			totalScore = totalScore + score;
		}
		else if (Input.GetKeyDown ("2"))
		{
			score = 2;
			totalScore = totalScore + score;
		}
		else if (Input.GetKeyDown ("3"))
		{
			score = 3;
			totalScore = totalScore + score;
		}
		else if (Input.GetKeyDown ("4"))
		{
			score = 4;
			totalScore = totalScore + score;
		}
		else if (Input.GetKeyDown ("5"))
		{
			score = 5;
			totalScore = totalScore + score;
		}
	}

	IEnumerator LoseTime ()
	{
		while (true)
		{
			yield return new WaitForSeconds (1);
			timeLeft--;
		}
	}
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using RootMotion.FinalIK;
//using NoSqlite.Orm;
using UnityEngine;

public class PlayerStateRecording : MonoBehaviour
{
	GUITextControl textControl;

	private bool isInit;
	private bool isRecording;

//	private DataService ds;

	public SessionDefinition session;
	public GestureDefinition gesture;
	public PlayerStateDefinition state;

	private GameObject playerObject;
	private Transform headTransform;
	private Transform leftHandTransform;
	private Transform rightHandTransform;
	private NVRHand leftHand;
	private NVRHand rightHand;

	private GameObject IKObject;
	private VRIK IK;
	private Transform[] skeletalTransforms;

	private IndividualDataCollectionController controller;

	private JobQueue<InsertDataJob> queue;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{
		isInit = false;
		session = new SessionDefinition ();
		state = new PlayerStateDefinition ();

		textControl = GameObject.Find ("Text").GetComponent<GUITextControl> ();

		controller = GameObject.FindGameObjectWithTag ("MImEGlobalObject").GetComponent<IndividualDataCollectionController> ();

//		Debug.Log ("Started");
//		InitializeDatabase ();
//		Invoke ("InitializeDatabase", 2);

		queue = new JobQueue<InsertDataJob> (2);

		Invoke ("InitializeTracking", 5);

//		List<PlayerStateDefinition> stateDefs = new List<PlayerStateDefinition> ();
//		stateDefs.Add (new PlayerStateDefinition (System.DateTime.Now.ToString (), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), true, true, true, true, new Vector3Definition[22], new Vector4Definition[22]));
//		stateDefs.Add (new PlayerStateDefinition (System.DateTime.Now.ToString (), new Vector3Definition (1, 2, 4), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), true, true, true, true, new Vector3Definition[22], new Vector4Definition[22]));
//		stateDefs.Add (new PlayerStateDefinition (System.DateTime.Now.ToString (), new Vector3Definition (1, 2, 5), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), true, true, true, true, new Vector3Definition[22], new Vector4Definition[22]));
//
//		Debug.Log ("List JSON: " + JsonUtility.ToJson (stateDefs));
//
//		string stateString = JsonUtility.ToJson (new PlayerStateDefinition (System.DateTime.Now.ToString (), new Vector3Definition (3, 4, 5), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), true, true, true, true, new Vector3Definition[22], new Vector4Definition[22]));
//		Debug.Log ("State JSON: " + stateString);
//		PlayerStateDefinition testState = JsonUtility.FromJson<PlayerStateDefinition> (stateString);
//		Debug.Log ("Reconstituted State JSON: " + JsonUtility.ToJson (testState));
	}

	void InitializeDatabase ()
	{
//		ds = new DataService ("PlayerStateRecordings.db");
//		ds.CreateTable<GestureDefinition> ();
//		ds.CreateTable<SessionDefinition> ();

//		try
//		{
//			var db = NoSqliteScript.Get("propsgame");
//			db.CreateTable<GestureDefinition> (false);
//			db.CreateTable<SessionDefinition> (false);
//		}
//		catch (Exception ex)
//		{
//			Debug.Log ("Initialization Exception.");
//			Debug.LogException(ex);
//		}

//		Debug.Log ("Database Initialized");
//		textControl.SetTimedMessage ("Database Initialized", 1);
	}

	/// <summary>
	/// Initializes the tracking.
	/// </summary>
	void InitializeTracking ()
	{
		playerObject = GameObject.Find ("NVRPlayer");
		leftHand = playerObject.transform.Find ("LeftHand").GetComponent<NVRHand> ();
		rightHand = playerObject.transform.Find ("RightHand").GetComponent<NVRHand> ();
		headTransform = playerObject.transform.Find ("Head").transform;
		leftHandTransform = playerObject.transform.Find ("LeftHand").transform;
		rightHandTransform = playerObject.transform.Find ("RightHand").transform;

		IKObject = GameObject.FindGameObjectWithTag ("PlayerAvatar");
		IK = IKObject.GetComponent<VRIK> ();
		//Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
		skeletalTransforms = IK.references.GetTransforms ();

		Debug.Log ("Tracking Initialized");
		textControl.SetTimedMessage ("Tracking Initialized", 1);

		isInit = true;
	}

	/// <summary>
	/// Fixed update method.
	/// </summary>
	void FixedUpdate ()
	{
		if (isInit)
		{
			HandleInput ();
			queue.Update ();
		}

		if (isInit && isRecording)
		{

			Vector3Definition[] jointPositions = new Vector3Definition[22];
			Vector4Definition[] jointRotations = new Vector4Definition[22];
			for (int i = 0; i < 22; i++)
			{
				if (skeletalTransforms == null || skeletalTransforms [i] == null)
				{
					jointPositions [i] = new Vector3Definition ();
					jointRotations [i] = new Vector4Definition ();
					continue;
				}

				jointPositions [i] = new Vector3Definition (skeletalTransforms [i].position);
				jointRotations [i] = new Vector4Definition (skeletalTransforms [i].rotation);
			}

			state = new PlayerStateDefinition ();
			state.timestamp = System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff");
			state.headPosition = new Vector3Definition (headTransform.position);
			state.headRotation = new Vector4Definition (headTransform.rotation);
			state.leftHandPosition = new Vector3Definition (leftHandTransform.position);
			state.leftHandRotation = new Vector4Definition (leftHandTransform.rotation);
			state.rightHandPosition = new Vector3Definition (rightHandTransform.position);
			state.rightHandRotation = new Vector4Definition (rightHandTransform.rotation);

			state.objectPosition = new Vector3Definition(controller.GetCurrentProp ().transform.GetChild (0).gameObject.transform.position);
			state.objectRotation = new Vector4Definition(controller.GetCurrentProp ().transform.GetChild (0).gameObject.transform.rotation);

			state.leftControllerGripButtonDown = leftHand.HoldButtonDown;
			state.leftControllerTriggerButtonDown = leftHand.UseButtonDown;
			state.rightControllerGripButtonDown = rightHand.HoldButtonDown;
			state.rightControllerTriggerButtonDown = rightHand.UseButtonDown;

			state.leftControllerGripButtonUp = leftHand.HoldButtonUp;
			state.leftControllerTriggerButtonUp = leftHand.UseButtonUp;
			state.rightControllerGripButtonUp = rightHand.HoldButtonUp;
			state.rightControllerTriggerButtonUp = rightHand.UseButtonUp;
				
			state.skeletalPositions = jointPositions;
			state.skeletalRotations = jointRotations;

			gesture.AddState (state);

//			Debug.Log ("State: " + JsonUtility.ToJson (state));
		}
	}

	//TODO: MOVE TO CENTRAL KEYCONTROLLER
	void HandleInput()
	{
		if (Input.GetKeyUp (KeyCode.Space))
		{
			if (!isRecording)
			{
				gesture = new GestureDefinition ();
				gesture.objectName = controller.GetCurrentPropName ();
				gesture.objectSize = controller.GetCurrentPropSize ();
				gesture.objectStartingPosition = new Vector3Definition(controller.GetCurrentProp ().transform.GetChild (0).gameObject.transform.position);
				gesture.objectStartingRotation = new Vector4Definition(controller.GetCurrentProp ().transform.GetChild (0).gameObject.transform.rotation);


				Debug.Log ("Recording Started...");
				textControl.SetTimedMessage ("Recording Started...", 1);
				isRecording = true;
			}
			else
			{
				isRecording = false;

				Debug.Log ("Recording Stopped...");
				textControl.SetTimedMessage ("Recording stopped...", 1);

				session.AddGestureTimeStamp (gesture.timestamp);

				//ds.InsertOrReplaceObject<GestureDefinition> (gesture);

//				Debug.Log ("Gesture: " + gesture.Serialize ());

//				var db = NoSqliteScript.Get ("propsgame");
//				db.Insert<GestureDefinition> (gesture);
//				System.IO.File.WriteAllText (System.IO.Path.Combine (Application.streamingAssetsPath, System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss") + ".gesture"), gesture.Serialize ());

				queue.AddJob (new InsertDataJob(gesture, Application.streamingAssetsPath));

//				GestureDefinition newGesture = new GestureDefinition ();
//				newGesture.gestureID = gesture.gestureID;
//				newGesture = db.Select<GestureDefinition> (newGesture);
//				Debug.Log ("Retrieved Gesture: " + newGesture.Serialize ());
//				ListGenericDefinition<GestureDefinition> gestures = new ListGenericDefinition<GestureDefinition> (db.List<GestureDefinition> ());
			}
		}
	}

	void OnApplicationQuit()
	{
//		var db = NoSqliteScript.Get ("propsgame");
		if (isInit && isRecording)
		{
			session.AddGestureTimeStamp (gesture.timestamp);

//			ds.InsertOrReplaceObject<GestureDefinition> (gesture);

//			db.Insert<GestureDefinition> (gesture);
//			System.IO.File.WriteAllText (System.IO.Path.Combine (Application.streamingAssetsPath, System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss") + ".gesture"), gesture.Serialize ());

			queue.AddJob (new InsertDataJob(gesture, Application.streamingAssetsPath));
//			Debug.Log ("Gesture: " + gesture.Serialize ());

			Debug.Log ("Recording Stopped...");
			textControl.SetTimedMessage ("Recording stopped...", 1);

			isRecording = false;
		}

		if (gesture.playerStates.Count == 0)
		{
			return;
		}

//		ds.InsertOrReplaceObject<SessionDefinition> (session);
//		db.Insert<SessionDefinition> (session);
		System.IO.File.WriteAllText (System.IO.Path.Combine (Application.streamingAssetsPath, System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff") + ".session"), session.Serialize ());
//		Debug.Log ("Session: " + JsonUtility.ToJson (session));

//		SessionDefinition newSession = new SessionDefinition ();
//		newSession.sessionID = session.sessionID;
//		newSession = db.Select<SessionDefinition> (newSession);
//		Debug.Log ("Retrieved Session: " + newSession.Serialize ());
	}

	public class InsertDataJob : JobItem
	{
		string path;
		GestureDefinition gesture;

		public InsertDataJob(GestureDefinition gesture, string path)
		{
			this.gesture = gesture;
			this.path = path;
		}

		protected override void DoWork()
		{
			System.IO.File.WriteAllText (System.IO.Path.Combine (path, System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff") + ".gesture"), gesture.Serialize ());
//			var db = NoSqliteScript.Get ("propsgame");
//			db.Insert<GestureDefinition> (gesture);
			//			Debug.Log("Message at SendOSCMessageJob.DoWork(): " + m.Address + ", " + m.Values[0]);
		}

		public override void OnFinished()
		{
			
			//			Debug.Log("Message at SendOSCMessageJob.onFinished(): " + "/" + MImEOSCConstants.MImEToGENIIAddress + ", " + messageValue);
		}
	}
}

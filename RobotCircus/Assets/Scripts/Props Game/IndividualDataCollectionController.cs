﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using NewtonVR;

public class IndividualDataCollectionController : MonoBehaviour
{
	private int currentObjectIndex = 0;
	private Dictionary<GameObject, string> gameObjectDictionary;
	private List<GameObject> gameObjectKeyList;
	private bool isInit;
	public Transform propSpawnLocation;
	private float[] localScales = new float[] { 0.25f, 0.5f, 1f };

	private GameObject playerObject;
	private NVRHand leftHand;
	private NVRHand rightHand;

	// Use this for initialization
	void Start ()
	{
		//StartCoroutine("LoseTime");
		gameObjectDictionary = new Dictionary<GameObject, string> ();
		gameObjectKeyList = new List<GameObject> ();
		gameObjectKeyList.AddRange (GameObject.FindGameObjectsWithTag ("ObjectStateTracker"));
		foreach (GameObject gameObj in gameObjectKeyList)
		{
			gameObjectDictionary.Add (gameObj, gameObj.name);
		}

		propSpawnLocation = GameObject.Find ("PropSpawnLocation").transform;

		playerObject = GameObject.Find ("NVRPlayer");
		leftHand = playerObject.transform.Find ("LeftHand").GetComponent<NVRHand> ();
		rightHand = playerObject.transform.Find ("RightHand").GetComponent<NVRHand> ();

		isInit = false;
		Invoke ("LateStart", 0.5f);
	}

	void LateStart ()
	{
		foreach (GameObject gameObj in gameObjectKeyList)
		{
			gameObj.SetActive (false);
		}

		gameObjectKeyList [currentObjectIndex].transform.position = propSpawnLocation.position;
		gameObjectKeyList [currentObjectIndex].transform.GetChild (0).gameObject.transform.position = new Vector3 (0,0,0);
		gameObjectKeyList [currentObjectIndex].transform.GetChild (0).gameObject.GetComponent<Renderer> ().material.SetColor ("_Color", Random.ColorHSV (0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));
		gameObjectKeyList [0].SetActive (true);
		isInit = true;
	}

	// Update is called once per frame
	void Update ()
	{

		if (isInit)
		{
			if (Input.GetKeyDown ("right") || rightHand.UseButtonDown)
			{
				gameObjectKeyList [currentObjectIndex].SetActive (false);
				currentObjectIndex = currentObjectIndex + 1;
				if (currentObjectIndex >= gameObjectKeyList.Count)
				{
					currentObjectIndex = 0;
				}
				gameObjectKeyList [currentObjectIndex].transform.position = propSpawnLocation.position;
				gameObjectKeyList [currentObjectIndex].transform.GetChild (0).gameObject.transform.position = new Vector3 (0,0,0);
				gameObjectKeyList [currentObjectIndex].transform.GetChild (0).gameObject.GetComponent<Renderer> ().material.SetColor ("_Color", Random.ColorHSV (0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));
				int scalesIndex = Random.Range (0, 3);
				gameObjectKeyList [currentObjectIndex].transform.GetChild (0).localScale = new Vector3 (localScales [scalesIndex], localScales [scalesIndex], localScales [scalesIndex]);
				gameObjectKeyList [currentObjectIndex].SetActive (true);
			}

			if (Input.GetKeyDown ("left") || leftHand.UseButtonDown)
			{
				gameObjectKeyList [currentObjectIndex].transform.position = propSpawnLocation.position;
				gameObjectKeyList [currentObjectIndex].transform.GetChild (0).gameObject.transform.position = new Vector3 (0,0,0);
			}

//			if (Input.GetKeyDown ("left"))
//			{
//				gameObjectKeyList [currentObjectIndex].SetActive (false);
//				currentObjectIndex = currentObjectIndex - 1;
//				if (currentObjectIndex < 0)
//				{
//					currentObjectIndex = gameObjectKeyList.Count - 1;
//				}
//				gameObjectKeyList [currentObjectIndex].transform.position = propSpawnLocation.position;
//				gameObjectKeyList [currentObjectIndex].transform.GetChild (0).gameObject.transform.position = new Vector3 (0,0,0);
//				gameObjectKeyList [currentObjectIndex].transform.GetChild (0).gameObject.GetComponent<Renderer> ().material.SetColor ("_Color", Random.ColorHSV (0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));
//				int scalesIndex = Random.Range (0, 3);
//				gameObjectKeyList [currentObjectIndex].transform.GetChild (0).localScale = new Vector3 (localScales [scalesIndex], localScales [scalesIndex], localScales [scalesIndex]);
//				gameObjectKeyList [currentObjectIndex].SetActive (true);
//			}
		}
	}

	public GameObject GetCurrentProp()
	{
		return gameObjectKeyList [currentObjectIndex];
	}

	public string GetCurrentPropName()
	{
		return gameObjectDictionary [gameObjectKeyList [currentObjectIndex]];
	}

	public string GetCurrentPropSize()
	{
		GameObject prop = GetCurrentProp ();
		if (prop.transform.GetChild (0).transform.localScale.x == 0.25f && prop.transform.GetChild (0).transform.localScale.y == 0.25f && prop.transform.GetChild (0).transform.localScale.z == 0.25f)
		{
			return "SMALL";
		}
		else if (prop.transform.GetChild (0).transform.localScale.x == 0.5f && prop.transform.GetChild (0).transform.localScale.y == 0.5f && prop.transform.GetChild (0).transform.localScale.z == 0.5f)
		{
			return "MEDIUM";
		}
		else if (prop.transform.GetChild (0).transform.localScale.x == 1f && prop.transform.GetChild (0).transform.localScale.y == 1f && prop.transform.GetChild (0).transform.localScale.z == 1f)
		{
			return "LARGE";
		}
		else
		{
			return "UNKNOWN";
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using UnityEngine;

public class GlobalInteractableObjectManager : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		GameObject[] gameObjects = GameObject.FindGameObjectsWithTag ("ObjectStateTracker");
		//TODO: In logic below only looks at first child. Make this recurse over all Children!
		foreach (GameObject gameObject in gameObjects)
		{
			if (gameObject.transform.childCount == 0)
			{
				continue;
			}

			if (gameObject.transform.GetChild (0).gameObject.GetComponent<Rigidbody> () == null)
			{
				gameObject.transform.GetChild (0).gameObject.AddComponent<Rigidbody> ();
			}

			if (gameObject.transform.GetChild (0).gameObject.GetComponent<ReverseNormals> () == null)
			{
				gameObject.transform.GetChild (0).gameObject.AddComponent<ReverseNormals> ();
			}

			if (gameObject.transform.GetChild (0).gameObject.GetComponent<Collider> () == null)
			{
				gameObject.transform.GetChild (0).gameObject.AddComponent<MeshCollider> ();
				gameObject.transform.GetChild (0).gameObject.GetComponent<MeshCollider> ().convex = true;
				gameObject.transform.GetChild (0).gameObject.GetComponent<MeshCollider> ().inflateMesh = true;
				gameObject.transform.GetChild (0).gameObject.GetComponent<MeshCollider> ().skinWidth = 0.01f;
			}

			if (gameObject.transform.GetChild (0).gameObject.GetComponent<NVRInteractableItem> () == null)
			{
				gameObject.transform.GetChild (0).gameObject.AddComponent<NVRInteractableItem> ();
			}
		}
	}
}

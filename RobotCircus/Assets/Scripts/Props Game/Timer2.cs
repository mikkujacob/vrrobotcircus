﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Timer2 : MonoBehaviour
{
	public int timeLeft = 20;
	public Text countdownText;
	public int score = 0;
	public int totalScore = 0;
	public int numObjects = 0;
	public Dictionary<GameObject, string> gameObjectArray = new Dictionary<GameObject, string>(); 
	List<GameObject> k;
	bool scoreBoard=false;
	bool complete = false;

	// Use this for initialization
	void Start()
	{
		//StartCoroutine("LoseTime");
	
		gameObjectArray.Add (GameObject.Find ("Sphere"), "Instruction...sphere");
		gameObjectArray.Add (GameObject.Find ("Cylinder"), "Instruction...cylinder");
		gameObjectArray.Add (GameObject.Find ("Capsule"), "Instruction...capsule");
		gameObjectArray.Add (GameObject.Find ("Cube"), "Instruction...cube");
		k = new List<GameObject> (gameObjectArray.Keys);
		foreach (GameObject gameObject in k) {
			gameObject.SetActive (false);
		}

		k [0].SetActive (true);

	}

	// Update is called once per frame
	void Update()
	{

		if (Input.GetKeyDown ("down")) {
			StopCoroutine ("LoseTime");
			timeLeft = 20;
			numObjects = 0;
			score = 0;
			totalScore = 0;
			scoreBoard = false;
			complete = false;
			foreach (GameObject gameObject in k) {
				gameObject.SetActive (false);
			}

			k [0].SetActive (true);

			StartCoroutine ("LoseTime");


		}


		if (complete) {
			k [numObjects].SetActive (false);
			countdownText.text = ("Total Score: " + totalScore);
		} else {
			if (scoreBoard) {
				countdownText.text = ("Score: " + score + "\nTime Left = " + timeLeft);
			} else {

				countdownText.text = ("Instruction: " + gameObjectArray [k [numObjects]] + "\nTime Left = " + timeLeft);
			}
		}



		if ((timeLeft <= 0) && scoreBoard) {
			StopCoroutine ("LoseTime");
			k[numObjects].SetActive (false);
			numObjects = numObjects + 1;
			if (numObjects >= k.Count) {
				numObjects = numObjects - 1;
				complete = true;
			}
			k[numObjects].SetActive (true);
			timeLeft = 20;
			scoreBoard = false;
			score = 0;
			StartCoroutine ("LoseTime");
		} else if ((timeLeft <= 0) && !scoreBoard) {
			StopCoroutine ("LoseTime");
			timeLeft = 5;
			scoreBoard = true;
			StartCoroutine ("LoseTime");
		}




		if (Input.GetKeyDown ("right")) {
			StopCoroutine ("LoseTime");
			k[numObjects].SetActive (false);
			numObjects = numObjects + 1;
			if (numObjects >= k.Count) {
				numObjects = 0;
			}
			k[numObjects].SetActive (true);
			timeLeft = 20;
			scoreBoard = false;
			score = 0;
			StartCoroutine ("LoseTime");
		}

		if (Input.GetKeyDown ("left")) {
			StopCoroutine ("LoseTime");
			k[numObjects].SetActive (false);
			numObjects = numObjects - 1;
			if (numObjects < 0) {
				numObjects = k.Count - 1;
			}
			k[numObjects].SetActive (true);
			timeLeft = 20;
			scoreBoard = false;
			score = 0;
			StartCoroutine ("LoseTime");
		}

		if (Input.GetKeyDown ("1")) {
			score = 1;
			totalScore = totalScore + score;
		} else if (Input.GetKeyDown ("2")) {
			score = 2;
			totalScore = totalScore + score;
		} else if (Input.GetKeyDown ("3")) {
			score = 3;
			totalScore = totalScore + score;
		} else if (Input.GetKeyDown ("4")) {
			score = 4;
			totalScore = totalScore + score;
		} else if (Input.GetKeyDown ("5")) {
			score = 5;
			totalScore = totalScore + score;
		}

	
		
	
	}

	IEnumerator LoseTime()
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			timeLeft--;
		}
	}
}
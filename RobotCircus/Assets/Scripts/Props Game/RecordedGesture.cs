﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordedGesture
{
	private GestureDefinition gesture;
	private DateTime playStartTime;
	private TimeSpan currentTimeSpan;
	private SortedList<TimeSpan, PlayerStateDefinition> gestureFrames;
	public bool isInit;
	private IEnumerator<KeyValuePair<TimeSpan, PlayerStateDefinition>> gestureFrameEnumerator;
	public bool isPlaying;
	public bool isLooping;

	public RecordedGesture(string fileName)
	{
		gesture = new GestureDefinition ();
		gestureFrames = new SortedList<TimeSpan, PlayerStateDefinition> ();
		isInit = false;
		isPlaying = false;

		isInit = LoadGesture (fileName);
	}

	public bool LoadGesture(string fileName)
	{
		int errors = 0;

		try
		{
			string gestureContent = System.IO.File.ReadAllText (fileName);
			gesture = (GestureDefinition) (gesture.Deserialize (gestureContent));

			DateTime startTime = DateTime.ParseExact (gesture.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
			foreach(PlayerStateDefinition state in gesture.playerStates)
			{
				DateTime timeStamp = DateTime.ParseExact (state.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
				TimeSpan time = timeStamp.Subtract (startTime);
//				Debug.Log ("Timespan: " + time + ", DateTime: " + timeStamp.ToString ("yyyy-MM-dd-HH-mm-ss-fff"));
				if(gestureFrames.ContainsKey(time))
				{
					Debug.Log("Duplicate Key: " + time);
					errors++;
					continue;

				}
				gestureFrames.Add (time, state);
			}

			gestureFrameEnumerator = gestureFrames.GetEnumerator ();
		}
		catch (Exception e)
		{
			Debug.LogException (e);
			Debug.Log ("Recorded gesture failed to load.");
			return false;
		}

		Debug.Log ("Recorded gesture loaded with " + errors + " corrupted frames.");

		return true;
	}

	public void Play()
	{
		playStartTime = System.DateTime.Now;
		currentTimeSpan = TimeSpan.MinValue;
		isPlaying = true;
	}

	public void Stop()
	{
		playStartTime = System.DateTime.MinValue;
		currentTimeSpan = TimeSpan.MinValue;
		isPlaying = false;
	}

	public GestureDefinition GetGesture()
	{
		if (isInit)
		{
			return gesture;
		}
		else
		{
			return null;
		}
	}

	public PlayerStateDefinition GetNextFrame()
	{
		try
		{
			if(isInit && isPlaying)
			{
				if(gestureFrameEnumerator.MoveNext ())
				{
					currentTimeSpan = gestureFrameEnumerator.Current.Key;
					return gestureFrameEnumerator.Current.Value;
				}
				else if(isLooping)
				{
					gestureFrameEnumerator.Reset ();
					if(gestureFrameEnumerator.MoveNext ())
					{
						currentTimeSpan = gestureFrameEnumerator.Current.Key;
						return gestureFrameEnumerator.Current.Value;
					}
				}
			}

		}
		catch(Exception e)
		{
			currentTimeSpan = TimeSpan.MinValue;
			return null;
		}

		currentTimeSpan = TimeSpan.MinValue;
		return null;
	}

	public PlayerStateDefinition GetNearestFrame(DateTime time)
	{
		if (!isPlaying)
		{
			return null;
		}
		return GetNearestFrameDelta (time.Subtract (playStartTime));
	}

	public PlayerStateDefinition GetNearestFrameDelta(TimeSpan delta)
	{
		if (!isPlaying)
		{
			return null;
		}
		currentTimeSpan = delta;
		return gestureFrames [FindNearestTimeSpan (delta)];
	}

	public PlayerStateDefinition GetNearestFrameInterpolated(DateTime time)
	{
		if (!isPlaying)
		{
			return null;
		}
		return GetNearestFrameDeltaInterpolated(time.Subtract (playStartTime));
	}

	public PlayerStateDefinition GetNearestFrameDeltaInterpolated(TimeSpan delta)
	{
		if (!isPlaying)
		{
			return null;
		}
		currentTimeSpan = delta;

		TimeSpan[] ftcTimes = FindFloorTargetCeilingTimeSpans (delta);

		if (ftcTimes[1] != TimeSpan.MinValue) // Exact match was found
		{
			return gestureFrames[ftcTimes[1]];
		}
		else if(ftcTimes[0] != TimeSpan.MinValue && ftcTimes[2] != TimeSpan.MinValue) // Exact match NOT found. Floor and ceiling found
		{
			double amount = (delta.Subtract (ftcTimes[0]).TotalMilliseconds) / (ftcTimes[2].Subtract (ftcTimes[0]).TotalMilliseconds);
			return PlayerStateDefinition.Interpolate (gestureFrames [ftcTimes [0]], gestureFrames [ftcTimes [2]], amount);
		}
		else if(ftcTimes[0] != TimeSpan.MinValue && ftcTimes[1] == TimeSpan.MinValue && ftcTimes[2] == TimeSpan.MinValue) // Exact match NOT found. Floor found
		{
			return gestureFrames [ftcTimes [0]];
		}
		else if(ftcTimes[0] == TimeSpan.MinValue && ftcTimes[1] == TimeSpan.MinValue && ftcTimes[2] != TimeSpan.MinValue) // Exact match NOT found. Ceiling found
		{
			return gestureFrames [ftcTimes [2]];
		}

		return null;
	}

	private TimeSpan FindNearestTimeSpan(TimeSpan targetTimeSpan)
	{
		try
		{
			if (targetTimeSpan <= gestureFrames.Keys [0]) // Target is less than or equal to first timespan
			{
				return gestureFrames.Keys [0]; // Return first element.
			}
			else if (gestureFrames.Keys [gestureFrames.Keys.Count - 1] <= targetTimeSpan) // Last timespan is less than or equal to target
			{
				return gestureFrames.Keys [gestureFrames.Keys.Count - 1]; // Return last element.
			}
			else if (gestureFrames.ContainsKey (targetTimeSpan)) // Target is found
			{
				return targetTimeSpan; // Return target.
			}

			List<TimeSpan> times = new List<TimeSpan>(gestureFrames.Keys);
			int targetIndex = times.BinarySearch (targetTimeSpan);

			// Not found. Result of BinarySearch is bitwise complement of next largest element.
			int ceilIndex = ~targetIndex;

			int floorIndex = ceilIndex - 1;

			TimeSpan floorTimeSpan = targetTimeSpan.Subtract (times [floorIndex]);
			TimeSpan ceilTimeSpan = times [ceilIndex].Subtract (targetTimeSpan);

			return (floorTimeSpan < ceilTimeSpan) ? times [floorIndex] : times [ceilIndex];
		}
		catch(Exception e)
		{
			return TimeSpan.MinValue;
		}
	}

	private TimeSpan[] FindFloorTargetCeilingTimeSpans(TimeSpan targetTimeSpan) //Returns array with floor, target, and ceiling states.
	{
		TimeSpan[] ftcTimes = new TimeSpan[3];

		try
		{
			if (targetTimeSpan <= gestureFrames.Keys [0]) // Target is less than or equal to first timespan
			{
				ftcTimes [0] = TimeSpan.MinValue;
				ftcTimes [1] = TimeSpan.MinValue;
				ftcTimes [2] = gestureFrames.Keys [0];
				return ftcTimes;
			}
			else if (gestureFrames.Keys [gestureFrames.Keys.Count - 1] <= targetTimeSpan) // Last timespan is less than or equal to target
			{
				ftcTimes [0] = gestureFrames.Keys [gestureFrames.Keys.Count - 1];
				ftcTimes [1] = TimeSpan.MinValue;
				ftcTimes [2] = TimeSpan.MinValue;
				return ftcTimes;
			}
			else if (gestureFrames.ContainsKey (targetTimeSpan))
			{
				ftcTimes [0] = TimeSpan.MinValue;
				ftcTimes [1] = targetTimeSpan;
				ftcTimes [2] = TimeSpan.MinValue;
				return ftcTimes;
			}

			List<TimeSpan> times = (List<TimeSpan>)gestureFrames.Keys;
			int targetIndex = times.BinarySearch (targetTimeSpan);

			// Not found. Result of BinarySearch is bitwise complement of next largest element.
			int ceilIndex = ~targetIndex;

			int floorIndex = ceilIndex - 1;

			ftcTimes [0] = times [floorIndex];
			ftcTimes [1] = TimeSpan.MinValue;
			ftcTimes [2] = times [ceilIndex];
			return ftcTimes;
		}
		catch(Exception e)
		{
			ftcTimes [0] = TimeSpan.MinValue;
			ftcTimes [1] = TimeSpan.MinValue;
			ftcTimes [2] = TimeSpan.MinValue;
			return ftcTimes;
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class MImEGENIIOSCBridge : MonoBehaviour
{
    public UDPPacketIO controller;
    public Osc handler;

	public JobQueue<SendOSCMessageJob> jobQueue;

	public void OnEnable()
	{
		jobQueue = new JobQueue<SendOSCMessageJob>(1);
	}

	public void OnDisable()
	{
		jobQueue.ShutdownQueue();
	}

    // Use this for initialization
    public void Start ()
    {
        controller.init(MImEOSCConstants.RemoteIP, MImEOSCConstants.SendToPort, MImEOSCConstants.ListenerPort);
        handler.init(controller);

        handler.SetAddressHandler("/" + MImEOSCConstants.GENIIToMImEAddress, MessageHandler);
    }

    // Update is called once per frame
    public void Update ()
    {
		jobQueue.Update();
//        if(Input.GetKeyDown("1"))
//        {
//            SendMessage("This is a text message. : / ; \" \" {\"key\":value}", "string");
//        }
//        if(Input.GetKeyDown("2"))
//        {
//            GameObject cube = GameObject.Find("Cube1");
//            ObjectStateTracker cubeState = cube.GetComponent<ObjectStateTracker>();
//            SendMessage(cubeState.stateJSON, "ObjectStateDefinition");
//        }
//        if(Input.GetKeyDown("3"))
//        {
//            GameObject cube = GameObject.Find("Cube2");
//            ObjectStateTracker cubeState = cube.GetComponent<ObjectStateTracker>();
//            SendMessage(cubeState.stateJSON, "ObjectStateDefinition");
//        }
//        if(Input.GetKeyDown("4"))
//        {
//            KinectStateDefinition kinectState = new KinectStateDefinition();
//            SendMessage(JsonUtility.ToJson(kinectState), "KinectStateDefinition");
//        }
        if(Input.GetKeyDown("1"))
        {
            GameObject cube = GameObject.Find("Cube1");
            ObjectStateTracker cubeState = cube.GetComponent<ObjectStateTracker>();
            cubeState.Activate();
        }
        if(Input.GetKeyDown("2"))
        {
            GameObject cube = GameObject.Find("Cube1");
            ObjectStateTracker cubeState = cube.GetComponent<ObjectStateTracker>();
            cubeState.Deactivate();
        }
        if(Input.GetKeyDown("3"))
        {
            GameObject cube = GameObject.Find("Cube2");
            ObjectStateTracker cubeState = cube.GetComponent<ObjectStateTracker>();
            cubeState.Activate();
        }
        if(Input.GetKeyDown("4"))
        {
            GameObject cube = GameObject.Find("Cube2");
            ObjectStateTracker cubeState = cube.GetComponent<ObjectStateTracker>();
            cubeState.Deactivate();
		}
		if(Input.GetKeyDown("5"))
		{
			GameObject sphere = GameObject.Find("Sphere1");
			//KinectStateTracker sphereState = sphere.GetComponent<KinectStateTracker>();
			//sphereState.Activate();
		}
		if(Input.GetKeyDown("6"))
		{
			GameObject sphere = GameObject.Find("Sphere1");
			//KinectStateTracker sphereState = sphere.GetComponent<KinectStateTracker>();
			//sphereState.Deactivate();
		}
		if(Input.GetKeyDown("7"))
		{
			GameObject cube = GameObject.Find("Container1");
			ObjectStateTracker containerState = cube.GetComponent<ObjectStateTracker>();
			containerState.Activate();
		}
		if(Input.GetKeyDown("8"))
		{
			GameObject cube = GameObject.Find("Container1");
			ObjectStateTracker containerState = cube.GetComponent<ObjectStateTracker>();
			containerState.Deactivate();
		}
		if(Input.GetKeyDown("9"))
		{
			Debug.Log("Starting Action.");
			GlobalTrackerManager trackerManager = gameObject.GetComponent<GlobalTrackerManager>();
			trackerManager.StartAction();
		}
		if(Input.GetKeyDown("0"))
		{
			Debug.Log("Ending Action.");
			GlobalTrackerManager trackerManager = gameObject.GetComponent<GlobalTrackerManager>();
			trackerManager.EndAction();
		}
    }

    public void SendMessage(string messageValue, string messageType)
    {
		jobQueue.AddJob(new SendOSCMessageJob(messageValue, messageType, handler));
    }

    public void MessageHandler(OscMessage oscMessage)
    {
        //TODO: Handle a message that MImE receives from GENII.
    }

	public class SendOSCMessageJob : JobItem
	{
		string messageValue;
		string messageType;
		Osc handler;

		public SendOSCMessageJob(string messageValue, string messageType, Osc handler)
		{
			this.messageValue = messageValue;
			this.messageType = messageType;
			this.handler = handler;
		}

		protected override void DoWork()
		{
			OscMessage m = new OscMessage();
			m.Address = "/" + MImEOSCConstants.MImEToGENIIAddress;
			m.Values.Add (messageValue);
			m.Values.Add (messageType);
			handler.Send (m);
//			Debug.Log("Message at SendOSCMessageJob.DoWork(): " + m.Address + ", " + m.Values[0]);
		}

		public override void OnFinished()
		{
//			Debug.Log("Message at SendOSCMessageJob.onFinished(): " + "/" + MImEOSCConstants.MImEToGENIIAddress + ", " + messageValue);
		}
	}
}

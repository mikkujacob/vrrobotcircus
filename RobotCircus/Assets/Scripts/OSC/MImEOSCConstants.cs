﻿using UnityEngine;

public class MImEOSCConstants
{
    public static readonly string RemoteIP = "127.0.0.1";
    public static readonly int SendToPort = 57133;
    public static readonly int ListenerPort= 57132;
    public static readonly string MImEToGENIIAddress = "MImEToGENII";
    public static readonly string GENIIToMImEAddress = "GENIIToMImE";
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NewtonVR;
using System;
using System.Linq;

public class InteractableMeshGun : NVRInteractableItem {
	public List<Transform> combinedMeshesTrans;
	public List<Transform> toBeCombined;
	[HideInInspector]
	public NVRHand hand;
	[HideInInspector]
	public bool isHeld = false;
	public GameObject visualizer;

	private Dictionary<GameObject, Material[]> materialStorage;
	public Material outlineMaterial;

	private SphereCollider trigger;

	public InteractableInformationTool infoTool;

	public List<GameObject> hadOpenCanvas;

	private GameObject nvrPlayer;





	// Use this for initialization
	void Start () {
		base.Start();
		trigger = GetComponent<SphereCollider>();
		visualizer.SetActive(false);
		trigger.enabled = false;
		combinedMeshesTrans = new List<Transform>();
		toBeCombined = new List<Transform>();
		materialStorage = new Dictionary<GameObject, Material[]>();
		hadOpenCanvas = new List<GameObject>();
		nvrPlayer = GameObject.FindGameObjectWithTag("NVR Player");
	}

	// Update is called once per frame
	void Update () {
		base.Update();
		if (isHeld)
		{

		} else
		{

		}
	}

	protected void OnTriggerEnter(Collider other)
	{

		if (other.gameObject.tag == "ObjectStateTracker")
		{


			if (!toBeCombined.Contains(other.transform))
			{
				Material[] oldMaterialArray = other.GetComponent<MeshRenderer>().materials;
				materialStorage.Add(other.gameObject, oldMaterialArray);
				Material[] outlineMaterialArray = new Material[oldMaterialArray.Length];
				for (int i = 0; i < oldMaterialArray.Length; i++) {
					outlineMaterialArray[i] = outlineMaterial;
				}
				other.GetComponent<MeshRenderer>().materials = outlineMaterialArray;
			}
			toBeCombined.Add(other.transform);



		}
		else if (ContainsTag(other.gameObject, "ObjectStateTracker"))
		{
			GameObject parentObject = FirstParentWithTag(other.gameObject, "ObjectStateTracker");
			if (parentObject != null)
			{


				if (!toBeCombined.Contains(parentObject.transform))
				{
					Material[] oldMaterialArray = parentObject.GetComponent<MeshRenderer>().materials;
					materialStorage.Add(parentObject.gameObject, oldMaterialArray);
					Material[] outlineMaterialArray = new Material[oldMaterialArray.Length];
					for (int i = 0; i < oldMaterialArray.Length; i++) {
						outlineMaterialArray[i] = outlineMaterial;
					}
					parentObject.GetComponent<MeshRenderer>().materials = outlineMaterialArray;
				}

				toBeCombined.Add(parentObject.transform);

			}
		}
	}

	private bool ContainsTag(GameObject target, string tag)
	{
		Transform targetTransform = target.transform;
		while (targetTransform.parent != null)
		{
			if (targetTransform.parent.tag == tag)
			{
				return true;
			}
			targetTransform = targetTransform.parent;
		}
		return false;
	}

	private GameObject FirstParentWithTag(GameObject target, string tag)
	{
		Transform targetTransform = target.transform;
		while (targetTransform.parent != null)
		{
			if (targetTransform.parent.tag == tag)
			{
				return targetTransform.parent.gameObject;
			}
			targetTransform = targetTransform.parent;
		}
		return null;
	}



	protected void OnTriggerExit(Collider other)
	{

		if (other.gameObject.tag == "ObjectStateTracker")
		{
			toBeCombined.Remove(other.transform);

			if (!toBeCombined.Contains(other.transform) && materialStorage.ContainsKey(other.gameObject))
			{
				other.GetComponent<MeshRenderer>().materials = materialStorage[other.gameObject];
				materialStorage.Remove(other.gameObject);
			}

		} else if (ContainsTag(other.gameObject, "ObjectStateTracker"))
		{
			GameObject parentObject = FirstParentWithTag(other.gameObject, "ObjectStateTracker");
			if (parentObject != null)
			{
				toBeCombined.Remove(parentObject.transform);

				if (!toBeCombined.Contains(parentObject.transform) && materialStorage.ContainsKey(parentObject.gameObject))
				{
				parentObject.GetComponent<MeshRenderer>().materials = materialStorage[parentObject.gameObject];
				materialStorage.Remove(parentObject.gameObject);
				}


			}
		}

	}


	public void OnUseButtonDownMethod()
	{
		List<Transform> toBeCombinedDistinct = new List<Transform> (toBeCombined.Distinct());
		if (toBeCombinedDistinct.Count < 2)
		{
			return;
		}


		NVRHand[] nvrHands = nvrPlayer.GetComponentsInChildren<NVRHand>();
		if (nvrHands.Length > 1)
		{
			foreach (NVRHand someHand in nvrHands) {
				if (someHand.CurrentlyInteracting == this)
				{
					continue;
				}
				someHand.EndInteraction(someHand.CurrentlyInteracting);
			}
		}



		foreach (Transform someTransform in toBeCombined)
		{
			someTransform.GetComponent<MeshRenderer>().materials = materialStorage[someTransform.gameObject];
		}
		//GameObject newObject = new GameObject();
		GameObject newObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
		newObject.GetComponent<Collider>().enabled = false;
		//newObject.transform.position = Vector3.zero;
		//newObject.transform.position = new Vector3 (-0.4f, 2.5f, -0.6f);
		//newObject.transform.localScale = new Vector3 (0.4f, 0.4f, 0.4f);


		//GameObject meshObject = new GameObject();
		//GameObject meshObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
		//meshObject.transform.position = new Vector3 (-0.4f, 2.5f, -0.6f);
		//meshObject.transform.localScale = new Vector3 (0.4f, 0.4f, 0.4f);
		//newObject.AddComponent<MeshRenderer>();
		Transform newTransform = newObject.transform;
		combinedMeshesTrans.Add(newTransform);
		string allObjects = "collection of: ";

		Vector3 total = new Vector3(0f, 0f, 0f);
		//meshObject.transform.SetParent(newTransform);

		foreach (Transform someTransform in toBeCombinedDistinct) {
			total += someTransform.position;
		}
		newTransform.position = total / toBeCombinedDistinct.Count;

		foreach (Transform someTransform in toBeCombinedDistinct) {

			allObjects += someTransform.name + ", ";
			someTransform.SetParent(newTransform);
		}

		newTransform.name = allObjects;
		newTransform.position = Vector3.zero;

		//begin convoluted process to only get the meshes from objects that have active mesh renderers

		MeshRenderer[] meshRenderersInChildrens = newObject.GetComponentsInChildren<MeshRenderer>();
		List<MeshFilter> tempFiltersList = new List<MeshFilter>();
		foreach (MeshRenderer someRenderer in meshRenderersInChildrens)
		{
			if (someRenderer.enabled)
			{
				tempFiltersList.Add(someRenderer.gameObject.GetComponent<MeshFilter>());
			}
		}

		MeshFilter[] filters = tempFiltersList.ToArray();
		//MeshFilter[] filters = newObject.GetComponentsInChildren<MeshFilter>();

		Mesh finalMesh = new Mesh();

		//CombineInstance[] combiners = new CombineInstance[filters.Length];
		List<CombineInstance> combiners = new List<CombineInstance>();

		List<Material> materialsList = new List<Material>();
		for (int i = 0; i < filters.Length; i++)
		{
			if (filters[i].transform == newTransform)
			{
				continue;
			}

			for (int j = 0; j < filters[i].sharedMesh.subMeshCount; j++)
			{
				/*combiners[i].subMeshIndex = j;
				combiners[i].mesh = filters[i].sharedMesh;
				combiners[i].transform = filters[i].transform.localToWorldMatrix;*/
				CombineInstance aCombiner = new CombineInstance();
				aCombiner.subMeshIndex = j;
				aCombiner.mesh = filters[i].sharedMesh;
				aCombiner.transform = filters[i].transform.localToWorldMatrix;
				combiners.Add(aCombiner);
			}

			//combiners[i].transform = filters[i].transform.worldToLocalMatrix;
			foreach (Material someMaterial in filters[i].gameObject.GetComponent<MeshRenderer>().materials) {
				materialsList.Add(someMaterial);

			}

		}

		finalMesh.CombineMeshes(combiners.ToArray(), false, true, false);

		newObject.GetComponent<MeshFilter>().sharedMesh = finalMesh;
		newObject.GetComponent<MeshRenderer>().materials = materialsList.ToArray();
		//meshObject.AddComponent<MeshFilter>().sharedMesh = finalMesh;
		//meshObject.AddComponent<MeshRenderer>().material = filters[0].gameObject.GetComponent<MeshRenderer>().material;


		newObject.tag = "ObjectStateTracker";
		newObject.AddComponent<ObjectStateTracker>().setGlobalTrackerManager(filters[1].gameObject.GetComponent<ObjectStateTracker>().globalTrackerManager);
		newObject.GetComponent<ObjectStateTracker>().globalTrackerManager.objectStateTrackers.Add(newObject.GetComponent<ObjectStateTracker>());
		newObject.layer = LayerMask.NameToLayer("ObjectStateTracker");
		newObject.GetComponent<ObjectStateTracker>().initializeObjectStateDefinition();
		newObject.GetComponent<ObjectStateTracker>().globalTrackerManager.idToName.Add(newObject.GetComponent<ObjectStateTracker>().state.id, newObject.name);


		bool childrenHadCanvas = false;
		bool childrenHadGraviy = false;




		for (int i = 0; i < newTransform.childCount; i++)
		{
			if (newTransform.GetChild(i).GetComponent<Rigidbody>().useGravity)
			{
				childrenHadGraviy = true;
			}
			if (infoTool != null)
			{
				if (infoTool.targetToInfoCanvas.ContainsKey(newTransform.GetChild(i).gameObject))
				{
					hadOpenCanvas.Add(newTransform.GetChild(i).gameObject);
					Destroy(infoTool.targetToInfoCanvas[newTransform.GetChild(i).gameObject]);
					infoTool.targetToInfoCanvas.Remove(newTransform.GetChild(i).gameObject);
					childrenHadCanvas = true;
				}
			}
			/*if (newTransform.GetChild(i).gameObject.GetComponent<CharacterJoint>() != null)
			{
				CharacterJoint tempjoint = newTransform.GetChild(i).gameObject.GetComponent<CharacterJoint>();
				if (newObject.GetComponent<CharacterJoint>() == null)
				{
					newObject.AddComponent<CharacterJoint>().anchor = tempjoint.anchor + newTransform.GetChild(i).position - newTransform.position;
					newObject.GetComponent<CharacterJoint>().axis = tempjoint.axis;

				}
				else
				{
					newObject.GetComponent<CharacterJoint>().anchor = (newObject.GetComponent<CharacterJoint>().anchor + newTransform.position + tempjoint.anchor + newTransform.GetChild(i).position) / 2;
					if (newObject.GetComponent<CharacterJoint>().axis != tempjoint.axis)
					{
						newObject.GetComponent<CharacterJoint>().axis = Vector3.zero;
					}
				}
				tempjoint.connectedBody = null;
			}*/
			if (newTransform.GetChild(i).gameObject.GetComponent<CharacterJoint>() != null)
			{
				newTransform.GetChild(i).gameObject.GetComponent<CharacterJoint>().connectedBody = newObject.GetComponent<Rigidbody>();
			}


			newTransform.GetChild(i).gameObject.GetComponent<MeshRenderer>().enabled = false;
			//newTransform.GetChild(i).gameObject.GetComponent<Collider>().enabled = false;
			newTransform.GetChild(i).gameObject.GetComponent<NVRInteractableItem>().enabled = false;
			newTransform.GetChild(i).gameObject.GetComponent<NVRObjectStateTrackerController>().enabled = false;
			newTransform.GetChild(i).gameObject.GetComponent<ObjectStateTracker>().enabled = false;
			Destroy(newTransform.GetChild(i).gameObject.GetComponent<Rigidbody>());
			newTransform.GetChild(i).gameObject.tag = "Untagged";
			newTransform.GetChild(i).gameObject.layer = 0;
		//newTransform.GetChild(i).gameObject.SetActive(false);
			newObject.GetComponent<ObjectStateTracker>().globalTrackerManager.objectStateTrackers.Remove(newTransform.GetChild(i).gameObject.GetComponent<ObjectStateTracker>());
		}

		Rigidbody newRigidbody = newObject.AddComponent<Rigidbody>();
		newObject.AddComponent<NVRInteractableItem>();
		newObject.AddComponent<NVRObjectStateTrackerController>();

		if (childrenHadGraviy)
		{
			newRigidbody.useGravity = true;
			newObject.GetComponent<NVRInteractableItem>().EnableGravityOnDetach = true;

		}
		else
		{
			newObject.GetComponent<NVRInteractableItem>().EnableGravityOnDetach = false;
			newRigidbody.useGravity = false;
			newRigidbody.drag = 5;
		}

		if (childrenHadCanvas)
		{
			infoTool.targetToInfoCanvas.Add(newObject, Instantiate(infoTool.infoCanvasPrefab));
			//infoTool.OnBeginInteractionMethod();
			AttachedHand.Player.GetComponent<NVRCanvasInput>().VeryDelayedCameraInit();
			if (infoTool.isHeld)
			{
				infoTool.targetToInfoCanvas[newObject].SetActive(true);
			}

		}





		newTransform.position = total / toBeCombinedDistinct.Count;

		toBeCombined = new List<Transform>();

	}


	public void OnBeginInteractionMethod()
	{
		isHeld = true;
		hand = AttachedHand;
		visualizer.SetActive(true);
		trigger.enabled = true;

	}
	public void OnEndInteractionMethod()
	{
		isHeld = false;
		visualizer.SetActive(false);
		hand.OnTriggerExit(trigger);
		trigger.enabled = false;
		foreach (Transform someTransform in toBeCombined.Distinct()) {
			someTransform.GetComponent<MeshRenderer>().materials = materialStorage[someTransform.gameObject];
		}
		materialStorage.Clear();
		toBeCombined.Clear();
		hand = null;
	}


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class ScrewdriverAttachJoint : NVRAttachJoint {
	private InteractableScrewdriver screwdriver;
	private Screw screw;

	// Use this for initialization
	void Start () {
		screwdriver = gameObject.GetComponent<InteractableScrewdriver>();
		if (screwdriver == null)
		{
			screwdriver = transform.parent.GetComponent<InteractableScrewdriver>();
		}
		if (screwdriver == null)
		{
			screwdriver = transform.parent.parent.GetComponent<InteractableScrewdriver>();
		}
	}


	public void OnTriggerStay (Collider other) {
		screw = other.gameObject.GetComponent<Screw>();
		if (screw != null && screw.gameObject.GetComponent<Rigidbody>() != null)
		{
			if (screwdriver.running && screw.screwedInPlace == false)
			{
				base.OnTriggerStay(other);
				//Debug.Log(other.gameObject.name);
				screw.DisableBody();
			}
			else if (IsAttached)
			{
				Detach();
				if (screw.screwedInPlace == false)
				{
					screw.AttemptLock();
				}
			}
		}
	}

	/*public void Detach()
	{
			AttachedPoint.Detached(this);
			if (AttachedItem.gameObject.GetComponent<Screw>() != null)
			{
				//AttachedItem.gameObject.GetComponent<Screw>().AttemptLock();
			}

			AttachedItem = null;
			AttachedPoint = null;
			//Debug.Log("ITS WORKING!!!!!!!!!!!!!!!!");
	}


	/// copied from parent class

	protected virtual void FixedUpdate()
	{
			if (IsAttached == true)
			{
					FixedUpdateAttached();
			}
	}

	protected virtual void FixedUpdateAttached()
	{
			float distance = Vector3.Distance(AttachedPoint.transform.position, this.transform.position);

			if (distance > DropDistance)
			{
					Detach();
			}
			else
			{
					AttachedPoint.PullTowards(this);
			}
	}*/
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class Screw : MonoBehaviour {

	public bool screwedInPlace = true;
	private Vector3 localStartingPosition;
	public float screwLength = 2.6f;
	private NVRInteractableItem nvrInteractableItem;
	private Collider[] body;
	private bool isHeldByScrewdriver;
	private NVRAttachPoint attachPoint;
	private ScrewBody screwBody;


	// Use this for initialization
	void Start ()
	{
		localStartingPosition = transform.localPosition;
		nvrInteractableItem = gameObject.GetComponent<NVRInteractableItem>();
		Collider[] childrenColliders = gameObject.GetComponentsInChildren<Collider>();
		List<Collider> bodyColliders = new List<Collider>();
		screwBody = GetComponentInChildren<ScrewBody>();

		foreach (Collider collider in childrenColliders) {
			if (collider.gameObject != gameObject)
			{
				bodyColliders.Add(collider);
			}
		}
		body = bodyColliders.ToArray();
		attachPoint = gameObject.GetComponent<NVRAttachPoint>();


		if (screwedInPlace)
		{
			Lock();
		}

	}

	private void Lock ()
	{
		nvrInteractableItem.enabled = false;
		GameObject.Destroy(gameObject.GetComponent<Rigidbody>());
		DisableBody();
	}

	// Update is called once per frame
	void Update ()
	{
		if (attachPoint.IsAttached == false && !screwedInPlace)
		{
			EnableBody();
		}

	}

	public void Loosen (InteractableScrewdriver screwdriver)
	{
		float offset = 0.0001f;
		transform.RotateAround(transform.position, transform.up, screwdriver.speed * Time.deltaTime);
		transform.Translate(transform.up * Time.deltaTime * screwdriver.speed * offset, Space.World);

		if (Vector3.Distance(localStartingPosition, transform.localPosition) > screwLength)
		{
			Release();
		}
	}

	public void Release ()
	{
		screwedInPlace = false;
		transform.parent = null;
		Rigidbody theRigidbody = gameObject.AddComponent<Rigidbody>();
		GetComponent<NVRAttachPoint>().Rigidbody = theRigidbody;
		nvrInteractableItem.Rigidbody = theRigidbody;
		nvrInteractableItem.enabled = true;
		nvrInteractableItem.Rigidbody = theRigidbody;
		nvrInteractableItem.UpdateColliders();
		GetComponent<NVRAttachPoint>().Item = nvrInteractableItem;
	}

	public void DisableBody ()
	{
		foreach (Collider collider in body) {
			collider.isTrigger = true;
		}
	}

	private void EnableBody ()
	{
		foreach (Collider collider in body) {
			collider.isTrigger = false;
		}
	}

	public void AttemptLock ()
	{
		bool successful = screwBody.combineUsingScrew(this);
		Lock();
		screwedInPlace = true;
	}
}

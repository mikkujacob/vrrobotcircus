﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class InteractableScrewdriver : NVRInteractableItem {

	private Screw screw;
	public bool running = false;
	private Collider trigger;

	public GameObject screwdriverHead;
	public float speed = 2.0f;


	// Use this for initialization
	void Start () {
		base.Start();
		trigger = findTriggerCollider(this.gameObject);
	}

	private Collider findTriggerCollider (GameObject gameObject)
	{
		Collider[] allColliders = gameObject.GetComponentsInChildren<Collider>();
		foreach (Collider collider in allColliders)
		{
			if (collider.isTrigger)
			{
				return collider;
			}
		}
		return null;
	}

	// Update is called once per frame
	void Update () {
		base.Update();
		if (running) {
			//do animation
			//screwdriverHead.GetComponent<Rigidbody>().AddTorque(screwdriverHead.transform.up * speed);
			screwdriverHead.transform.RotateAround(screwdriverHead.transform.position, screwdriverHead.transform.right, speed * Time.deltaTime);
		}
	}

	void OnTriggerEnter (Collider other)
	{
		//screw = other.gameObject.GetComponent<Screw>();
	}

	void OnTriggerExit (Collider other)
	{
		//screw = null;
	}

	void OnTriggerStay (Collider other)
	{
		Screw theScrew = other.gameObject.GetComponent<Screw>();
		if (running && theScrew != null && theScrew.screwedInPlace)
		{
			//float turn = theScrew.GetAxis("Horizontal");
      theScrew.Loosen(this);
		}
	}

	public void OnUseButtonDownMethod()
	{
		running = true;

	}

	public void OnUseButtonUpMethod()
	{
		running = false;

	}

	public void OnBeginInteractionMethod()
	{
		//trigger.enabled = true;
	}

	public void OnEndInteractionMethod()
	{
		//trigger.enabled = false;
		running = false;
	}
}

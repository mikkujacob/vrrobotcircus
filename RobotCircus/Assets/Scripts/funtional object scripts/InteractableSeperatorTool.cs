﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;
using System.Linq;

public class InteractableSeperatorTool : NVRInteractableItem {
	[HideInInspector]
	public NVRHand hand;
	[HideInInspector]
	public bool isHeld = false;

	public List<Transform> toBeSeperated;
	public GameObject visualizer;
	public InteractableMeshGun interactableMeshGun;
	public Material outlineMaterial;
	private Dictionary<GameObject, Material[]> materialStorage;

	private SphereCollider trigger;
	private GameObject nvrPlayer;


	// Use this for initialization
	void Start () {
		base.Start();
		toBeSeperated =  new List<Transform>();
		trigger = GetComponent<SphereCollider>();
		visualizer.SetActive(false);
		trigger.enabled = false;
		materialStorage = new Dictionary<GameObject, Material[]>();
		nvrPlayer = GameObject.FindGameObjectWithTag("NVR Player");
	}

	// Update is called once per frame
	void Update () {
		base.Update();
	}

	public void OnUseButtonDownMethod()
	{
		List<Transform> transformsToBeRemoved = new List<Transform>();


		NVRHand[] nvrHands = nvrPlayer.GetComponentsInChildren<NVRHand>();
		if (nvrHands.Length > 1)
		{
			foreach (NVRHand someHand in nvrHands) {
				if (someHand.CurrentlyInteracting == this)
				{
					continue;
				}
				someHand.EndInteraction(someHand.CurrentlyInteracting);
			}
		}


		foreach (Transform someTransform in toBeSeperated) {
			if (isACombinedObject(someTransform))
			{
				if (transformsToBeRemoved.Contains(someTransform))
				{
					continue;
				}
				if (interactableMeshGun.infoTool != null)
				{
					if (interactableMeshGun.infoTool.targetToInfoCanvas.ContainsKey(someTransform.gameObject))
					{
						Destroy(interactableMeshGun.infoTool.targetToInfoCanvas[someTransform.gameObject]);
						interactableMeshGun.infoTool.targetToInfoCanvas.Remove(someTransform.gameObject);
					}
				}
				interactableMeshGun.combinedMeshesTrans.Remove(someTransform);
				Transform[] children = someTransform.GetComponentsInChildren<Transform>();
				foreach (Transform childTransform in children)
				{
					if (childTransform.parent == someTransform)
					{


						childTransform.gameObject.AddComponent<Rigidbody>();
						childTransform.parent = null;
						childTransform.gameObject.GetComponent<MeshRenderer>().enabled = true;
						childTransform.gameObject.GetComponent<NVRInteractableItem>().enabled = true;
						if (childTransform.gameObject.GetComponent<NVRInteractableItem>().EnableGravityOnDetach == false)
						{
							childTransform.gameObject.GetComponent<Rigidbody>().useGravity = false;
							childTransform.gameObject.GetComponent<Rigidbody>().drag = 5;
						}

						if (childTransform.gameObject.GetComponent<CharacterJoint>() != null)
						{
							childTransform.gameObject.GetComponent<CharacterJoint>().connectedBody = childTransform.gameObject.GetComponent<Rigidbody>();
						}


						childTransform.gameObject.GetComponent<NVRInteractableItem>().WakeUp();
						childTransform.gameObject.GetComponent<NVRObjectStateTrackerController>().enabled = true;

						childTransform.gameObject.GetComponent<ObjectStateTracker>().enabled = true;


						childTransform.gameObject.tag = "ObjectStateTracker";
						childTransform.gameObject.layer = LayerMask.NameToLayer("ObjectStateTracker");;
						childTransform.gameObject.GetComponent<ObjectStateTracker>().globalTrackerManager.objectStateTrackers.Add(childTransform.gameObject.GetComponent<ObjectStateTracker>());

						if (interactableMeshGun.hadOpenCanvas.Contains(childTransform.gameObject))
						{
							interactableMeshGun.hadOpenCanvas.Remove(childTransform.gameObject);
							if (interactableMeshGun.infoTool != null)
							{
								interactableMeshGun.infoTool.targetToInfoCanvas.Add(childTransform.gameObject, Instantiate(interactableMeshGun.infoTool.infoCanvasPrefab));
								AttachedHand.Player.GetComponent<NVRCanvasInput>().VeryDelayedCameraInit();
								if (interactableMeshGun.infoTool.isHeld)
								{
									interactableMeshGun.infoTool.targetToInfoCanvas[childTransform.gameObject].SetActive(true);
								}
							}
						}
					}
				}
				//toBeSeperated.Remove(someTransform);
				transformsToBeRemoved.Add(someTransform);
				someTransform.GetComponent<ObjectStateTracker>().globalTrackerManager.objectStateTrackers.Remove(someTransform.GetComponent<ObjectStateTracker>());
				interactableMeshGun.toBeCombined.Remove(someTransform);
				//Destroy(someTransform.gameObject);
			}
		}

		foreach (Transform someTransform in transformsToBeRemoved) {
			while (toBeSeperated.Contains(someTransform))
			{
				toBeSeperated.Remove(someTransform);
			}

			Destroy(someTransform.gameObject);
		}

	}

	public bool isACombinedObject(Transform someTransform)
	{
		return interactableMeshGun.combinedMeshesTrans.Contains(someTransform);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "ObjectStateTracker" /*&& IsUnique(other.gameObject)*/)
		{

			if (!toBeSeperated.Contains(other.transform))
			{
				Material[] oldMaterialArray = other.GetComponent<MeshRenderer>().materials;
				materialStorage.Add(other.gameObject, oldMaterialArray);
				Material[] outlineMaterialArray = new Material[oldMaterialArray.Length];
				for (int i = 0; i < oldMaterialArray.Length; i++) {
					outlineMaterialArray[i] = outlineMaterial;
				}
				other.GetComponent<MeshRenderer>().materials = outlineMaterialArray;
			}

			toBeSeperated.Add(other.transform);

		} else if (ContainsTag(other.gameObject, "ObjectStateTracker"))
		{
			GameObject parentObject = FirstParentWithTag(other.gameObject, "ObjectStateTracker");
			if (parentObject != null /*&& IsUnique(parentObject)*/)
			{

				if (!toBeSeperated.Contains(parentObject.transform))
				{
					Material[] oldMaterialArray = parentObject.GetComponent<MeshRenderer>().materials;
					materialStorage.Add(parentObject.gameObject, oldMaterialArray);
					Material[] outlineMaterialArray = new Material[oldMaterialArray.Length];
					for (int i = 0; i < oldMaterialArray.Length; i++) {
						outlineMaterialArray[i] = outlineMaterial;
					}
					parentObject.GetComponent<MeshRenderer>().materials = outlineMaterialArray;
				}

				toBeSeperated.Add(parentObject.transform);
			}
		}
	}

	private bool ContainsTag(GameObject target, string tag)
	{
		Transform targetTransform = target.transform;
		while (targetTransform.parent != null)
		{
			if (targetTransform.parent.tag == tag)
			{
				return true;
			}
			targetTransform = targetTransform.parent;
		}
		return false;
	}

	private GameObject FirstParentWithTag(GameObject target, string tag)
	{
		Transform targetTransform = target.transform;
		while (targetTransform.parent != null)
		{
			if (targetTransform.parent.tag == tag)
			{
				return targetTransform.parent.gameObject;
			}
			targetTransform = targetTransform.parent;
		}
		return null;
	}


	private bool IsUnique (GameObject otherGameObject)
	{
		foreach (Transform someTransform in toBeSeperated)
		{
			if (someTransform.gameObject == otherGameObject)
			{
				return false;
			}
		}
		return true;
	}

	void OnTriggerExit(Collider other)
	{

		if (other.gameObject.tag == "ObjectStateTracker")
		{
			toBeSeperated.Remove(other.transform);
			if (!toBeSeperated.Contains(other.transform) && materialStorage.ContainsKey(other.gameObject))
			{
				other.GetComponent<MeshRenderer>().materials = materialStorage[other.gameObject];
				materialStorage.Remove(other.gameObject);
			}
		} else if (ContainsTag(other.gameObject, "ObjectStateTracker"))
		{
			GameObject parentObject = FirstParentWithTag(other.gameObject, "ObjectStateTracker");
			if (parentObject != null)
			{
				toBeSeperated.Remove(parentObject.transform);
				if (!toBeSeperated.Contains(parentObject.transform) && materialStorage.ContainsKey(parentObject.gameObject))
				{
				parentObject.GetComponent<MeshRenderer>().materials = materialStorage[parentObject.gameObject];
				materialStorage.Remove(parentObject.gameObject);
				}
			}
		}

	}


	public void OnBeginInteractionMethod()
	{
		isHeld = true;
		hand = AttachedHand;
		visualizer.SetActive(true);
		trigger.enabled = true;

	}
	public void OnEndInteractionMethod()
	{
		isHeld = false;
		visualizer.SetActive(false);
		hand.OnTriggerExit(trigger);
		trigger.enabled = false;
		foreach (Transform someTransform in toBeSeperated.Distinct()) {
			someTransform.GetComponent<MeshRenderer>().materials = materialStorage[someTransform.gameObject];
		}
		materialStorage.Clear();
		toBeSeperated.Clear();
		hand = null;

	}
}

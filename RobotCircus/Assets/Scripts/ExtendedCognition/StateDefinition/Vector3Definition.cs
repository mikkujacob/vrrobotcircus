﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Vector3Definition
{
	public float x;
	public float y;
	public float z;

    public Vector3Definition()
    {

    }

    public Vector3Definition(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3Definition(Vector3 location)
    {
        this.x = location.x;
        this.y = location.y;
        this.z = location.z;
    }

	public Vector3 ToVector3()
	{
		return new Vector3 (x, y, z);
	}
}

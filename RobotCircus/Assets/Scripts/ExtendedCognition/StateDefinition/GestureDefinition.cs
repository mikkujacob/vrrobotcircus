﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GestureDefinition 
{

	public string gestureID;

	public string timestamp;

	public string objectName;
	public string objectSize;
	public Vector3Definition objectStartingPosition;
	public Vector4Definition objectStartingRotation;

	public ListGenericDefinition<PlayerStateDefinition> playerStates;

	public GestureDefinition ()
	{
		gestureID = Guid.NewGuid ().ToString ();
		timestamp = System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff");
		objectName = "";
		objectSize = "";
		objectStartingPosition = new Vector3Definition ();
		objectStartingRotation = new Vector4Definition ();
		playerStates = new ListGenericDefinition<PlayerStateDefinition> ();
	}

	public GestureDefinition (string id, string timeString, string objName, string objSize, Vector3Definition objectPosition, Vector4Definition objectRotation, ListGenericDefinition<PlayerStateDefinition> states)
	{
		gestureID = id;
		timestamp = timeString;
		objectName = objName;
		objectSize = objSize;
		objectStartingPosition = objectPosition;
		objectStartingRotation = objectRotation;
		playerStates = new ListGenericDefinition<PlayerStateDefinition> (states);
	}

	public void AddState (PlayerStateDefinition state)
	{
		playerStates.Add (state);
	}

	public string Serialize()
	{
//		return JsonUtility.ToJson (this);

		string serialized = gestureID + "$" + timestamp + "$"  + objectName + "$" + objectSize + "$" + JsonUtility.ToJson (objectStartingPosition) + "$" + JsonUtility.ToJson (objectStartingRotation) + "$";

		foreach (PlayerStateDefinition state in playerStates)
		{
			serialized += JsonUtility.ToJson (state) + "^";
		}

		serialized = serialized.Substring (0, serialized.Length - 1);

		return serialized;
	}

	public object Deserialize(string stringObj)
	{
//		return JsonUtility.FromJson<GestureDefinition> (stringObj);

		string[] data = stringObj.Split ('$');
		string[] states = data [6].Split ('^');
		ListGenericDefinition<PlayerStateDefinition> stateDefs = new ListGenericDefinition<PlayerStateDefinition> ();
		foreach (string state in states)
		{
			stateDefs.Add (JsonUtility.FromJson<PlayerStateDefinition> (state));
		}

		return new GestureDefinition (data [0], data [1], data [2], data [3], JsonUtility.FromJson<Vector3Definition> (data [4]), JsonUtility.FromJson<Vector4Definition> (data [5]), stateDefs);
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
//using NoSqlite.Attributes;
//using NoSqlite.Helpers;
//using NoSqlite.Orm;
using UnityEngine;

[System.Serializable]
public class PlayerStateDefinition
{
	public string timestamp;

	public Vector3Definition headPosition;
	public Vector4Definition headRotation;

	public Vector3Definition leftHandPosition;
	public Vector4Definition leftHandRotation;

	public Vector3Definition rightHandPosition;
	public Vector4Definition rightHandRotation;

	public Vector3Definition objectPosition;
	public Vector4Definition objectRotation;

	public bool leftControllerGripButtonDown;
	public bool leftControllerTriggerButtonDown;

	public bool rightControllerGripButtonDown;
	public bool rightControllerTriggerButtonDown;

	public bool leftControllerGripButtonUp;
	public bool leftControllerTriggerButtonUp;

	public bool rightControllerGripButtonUp;
	public bool rightControllerTriggerButtonUp;

	//Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
	public Vector3Definition[] skeletalPositions;
	public Vector4Definition[] skeletalRotations;

	public PlayerStateDefinition ()
	{
		this.timestamp = System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff");
		this.headPosition = new Vector3Definition ();
		this.headRotation = new Vector4Definition ();
		this.leftHandPosition = new Vector3Definition ();
		this.leftHandRotation = new Vector4Definition ();
		this.rightHandPosition = new Vector3Definition ();
		this.rightHandRotation = new Vector4Definition ();
		this.skeletalPositions = new Vector3Definition[22];
		this.skeletalRotations = new Vector4Definition[22];
	}

	public PlayerStateDefinition 
	(
		string timestampString,
		Vector3Definition headPosition, 
		Vector4Definition headRotation, 
		Vector3Definition leftHandPosition, 
		Vector4Definition leftHandRotation, 
		Vector3Definition rightHandPosition,
		Vector4Definition rightHandRotation,
		bool leftControllerGripButtonDown,
		bool leftControllerTriggerButtonDown,
		bool rightControllerGripButtonDown,
		bool rightControllerTriggerButtonDown,
		bool leftControllerGripButtonUp,
		bool leftControllerTriggerButtonUp,
		bool rightControllerGripButtonUp,
		bool rightControllerTriggerButtonUp,
		//Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
		Vector3Definition[] skeletalPositions,
		Vector4Definition[] skeletalRotations
	)
	{
		this.timestamp = timestampString;
		this.headPosition = headPosition;
		this.headRotation = headRotation;
		this.leftHandPosition = leftHandPosition;
		this.leftHandRotation = leftHandRotation;
		this.rightHandPosition = rightHandPosition;
		this.rightHandRotation = rightHandRotation;
		this.leftControllerGripButtonDown = leftControllerGripButtonDown;
		this.leftControllerTriggerButtonDown = leftControllerTriggerButtonDown;
		this.rightControllerGripButtonDown = rightControllerGripButtonDown;
		this.rightControllerTriggerButtonDown = rightControllerTriggerButtonDown;
		this.leftControllerGripButtonUp = leftControllerGripButtonUp;
		this.leftControllerTriggerButtonUp = leftControllerTriggerButtonUp;
		this.rightControllerGripButtonUp = rightControllerGripButtonUp;
		this.rightControllerTriggerButtonUp = rightControllerTriggerButtonUp;
		this.skeletalPositions = skeletalPositions;
		this.skeletalRotations = skeletalRotations;
	}

	public string Serialize()
	{
		return JsonUtility.ToJson (this);
	}

	public object Deserialize(string stringObj)
	{
		return JsonUtility.FromJson<PlayerStateDefinition> (stringObj);
	}

	//TODO!!! STUB IMPLEMENTATION. NEEDS TO BE ACTUALLY INTERPOLATED!!!
	public static PlayerStateDefinition Interpolate(PlayerStateDefinition state1, PlayerStateDefinition state2, double amount)
	{
		if (amount < 0.5)
		{
			return state1;
		}
		else
		{
			return state2;
		}
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
//using NoSqlite.Attributes;
//using NoSqlite.Helpers;
//using NoSqlite.Orm;
using UnityEngine;

[System.Serializable]
public class SessionDefinition
{

	public string sessionID;

	public string timestamp;

	public ListGenericDefinition<string> gestureTimeStamps;

	public SessionDefinition ()
	{
		sessionID = Guid.NewGuid ().ToString ();

		timestamp = System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff");

		gestureTimeStamps = new ListGenericDefinition<string> ();
	}

	public SessionDefinition (string id, string timeString, List<string> gestureTimestampStringSet)
	{
		sessionID = id;

		timestamp = timeString;

		gestureTimeStamps = new ListGenericDefinition<string> (gestureTimestampStringSet);
	}

	public void AddGestureTimeStamp (string gestureTimeString)
	{
		gestureTimeStamps.Add (gestureTimeString);
	}

	public string Serialize()
	{
//		return JsonUtility.ToJson (this);
		string serialized = sessionID + "$" + timestamp + "$";
		foreach (string time in gestureTimeStamps)
		{
			serialized += time + "^";
		}
		serialized = serialized.Substring (0, serialized.Length - 1);
		return serialized;
	}

	public object Deserialize(string stringObj)
	{
//		return JsonUtility.FromJson<SessionDefinition> (stringObj);
		string[] data = stringObj.Split ('$');
		string[] times = data [2].Split ('^');
		ListGenericDefinition<string> timestamps = new ListGenericDefinition<string> (times);
		return new SessionDefinition (data [0], data [1], timestamps);
	}
}

using UnityEngine;
using System.Collections.Generic;
//using UnityEditorInternal;

/// <summary>
/// Containment tracker.
/// </summary>
public class ContainmentTracker : ITracker
{
	/// <summary>
	/// The tracker.
	/// </summary>
	private ObjectStateTracker tracker;

	/// <summary>
	/// The tracker manager.
	/// </summary>
	private GlobalTrackerManager trackerManager;

	/// <summary>
	/// The game object.
	/// </summary>
	private GameObject gameObject;

	/// <summary>
	/// The mesh bounds.
	/// </summary>
	private Bounds colliderBounds;

	/// <summary>
	/// The size.
	/// </summary>
	private float size;

	/// <summary>
	/// The last containing.
	/// </summary>
	private bool lastContaining;

	/// <summary>
	/// The last contains.
	/// </summary>
	private List<string> lastContains;

	/// <summary>
	/// Reference to the gameObject's collider
	/// </summary>
	Collider collider;


	/// <summary>
	/// Initializes a new instance of the <see cref="ContainmentTracker"/> class.
	/// </summary>
	/// <param name="tracker">Tracker.</param>
	public ContainmentTracker(ObjectStateTracker tracker)
	{
		this.tracker = tracker;
		this.gameObject = tracker.gameObject;
		this.trackerManager = GameObject.FindGameObjectWithTag("MImEGlobalObject").GetComponent<GlobalTrackerManager>();
	}

	/// <summary>
	/// Starts the tracker.
	/// </summary>
	public void StartTracker()
	{
		collider = gameObject.GetComponent<Collider>();
		if(collider == null)
		{
			throw new MissingComponentException();
		}
		colliderBounds = collider.bounds;
		size = colliderBounds.size.magnitude;

		lastContaining = tracker.state.containing;
		lastContains = new List<string>(tracker.state.contains);
	}

	/// <summary>
	/// Updates the tracker.
	/// </summary>
	public void UpdateTracker()
	{
		//        Debug.Log("ContainmentTracker.UpdateTracker()");
		colliderBounds = collider.bounds;
		Collider[] containingColliders = Physics.OverlapBox(colliderBounds.center, colliderBounds.extents, gameObject.transform.rotation, 1 << LayerMask.NameToLayer("ObjectStateTracker"), QueryTriggerInteraction.Collide);
		//Collider[] containingColliders = new Collider[0];
		//		Debug.Log("LayerMask: 1 << LayerMask.NameToLayer(\"ObjectStateTracker\"):" + (1 << LayerMask.NameToLayer("ObjectStateTracker")));

		//-Debug.Log("Colliders: " + containingColliders.Length);
		//-Debug.Log("gameObject.name(): " + gameObject.name);

		if(containingColliders.Length != 0)
		{
			lastContains.Clear();
			lastContains.AddRange(tracker.state.contains);
			lastContaining = tracker.state.containing;
			List<string> sameContains = new List<string>();
			List<string> addContains = new List<string>();
			List<string> removeContains = new List<string>();

			bool isContainsChanged = false;

			//for each collider inside OverlapBox
			foreach(Collider collider in containingColliders)
			{

				//-Debug.Log("collider.gameObject.name(): " + collider.gameObject.name);
				//-Debug.Log("this.size(): " + size);
				//-Debug.Log("collider.bounds.size(): " + collider.bounds.size.magnitude);
				//-Debug.Log("Vector3.Distance(collider.bounds.center, rendererBounds.center): " + Vector3.Distance(collider.bounds.center, rendererBounds.center));
				//-Debug.Log("collider.bounds.center: " + collider.bounds.center.ToString());
				//-Debug.Log("rendererBounds.center: " + rendererBounds.center.ToString());
				//-Debug.Log("(rendererBounds.extents - collider.bounds.extents).magnitude: " + (rendererBounds.extents - collider.bounds.extents).magnitude);
				//-Debug.Log("rendererBounds.extents: " + rendererBounds.extents.ToString());
				//-Debug.Log("collider.bounds.extents: " + collider.bounds.extents.ToString());

				//TODO: Check / test math here!!!
				//if((collider.bounds.size.magnitude < this.size) && (Vector3.Distance(collider.bounds.center, this.rendererBounds.center) < (this.rendererBounds.extents - collider.bounds.extents).magnitude)){...} //Positve version of if condition
				if(collider.bounds.size.magnitude >= size || collider.gameObject == tracker.gameObject /*|| Vector3.Distance(collider.bounds.center, rendererBounds.center) >= (rendererBounds.extents - collider.bounds.extents).magnitude*/) //Negative version of if condition
				{
					continue;
				}

				//The collider obtained is inside this rendererBounds.
				ObjectStateTracker stateTracker = collider.gameObject.GetComponent<ObjectStateTracker>();
				ObjectStateDefinition stateDefinition = stateTracker.state;

				//If additions to contains are detected
				if(!lastContains.Contains(stateDefinition.id))
				{
					isContainsChanged = true;
					addContains.Add(stateDefinition.id);
				}
				else
				{
					sameContains.Add(stateDefinition.id);
					tracker.state.contains.Remove(stateDefinition.id);
				}
			}

			//If there are removed elements
			if(tracker.state.contains.Count != 0)
			{
				isContainsChanged = true;

				removeContains.AddRange(tracker.state.contains);

				tracker.state.contains.Clear();
			}

			//Add all the contained objects to contains
			tracker.state.contains.AddRange(addContains);
			tracker.state.contains.AddRange(sameContains);

			//If this object was containing nothing previously
			if(!lastContaining && tracker.state.contains.Count > 0)
			{
				Debug.Log(tracker.gameObject.name + " was empty -> now containing " + tracker.state.contains.Count + ". change in " + tracker.gameObject.name);
				tracker.isChanged = true;
				tracker.state.containing = true;

				//-Debug.Log("Object is now containing something.");

				//Manage tracked changes update.
				tracker.state.changes.Add(new TrackedChange("containing", lastContaining.ToString(), tracker.state.containing.ToString()));
			}

			//if contains was changed in some way (additions or removals)
			if(isContainsChanged)
			{
				tracker.isChanged = true;
				Debug.Log(tracker.gameObject.name + " contains something new, had " + lastContains.Count + " now " + tracker.state.contains.Count + ". change in " + tracker.gameObject.name);
				//-Debug.Log("Object contains something new in some way.");

				//Manage tracked changes update.
				tracker.state.changes.Add(new TrackedChange("contains", lastContains.ToString(), tracker.state.contains.ToString()));
			}

			//For each object that was added, add this objects id to it as a container and mark it contained.
			foreach(string id in addContains)
			{
				ObjectStateTracker otherTracker = trackerManager.GetObjectStateTrackerFromObjectID(id);
				otherTracker.isChanged = true;
				Debug.Log(otherTracker.gameObject.name + " is contained in " + tracker.gameObject.name + ". change in " + otherTracker.gameObject.name);
				List<string> lastContainer = new List<string>(otherTracker.state.containers);
				otherTracker.state.containers.Add(tracker.state.id);
				otherTracker.state.changes.Add(new TrackedChange("containers", lastContainer.ToString(), otherTracker.state.containers.ToString()));

				//-Debug.Log("Other object has a new container.");

				bool lastContained = otherTracker.state.contained;
				otherTracker.state.contained = true;
				otherTracker.state.changes.Add(new TrackedChange("contained", lastContained.ToString(), otherTracker.state.contained.ToString()));


				//-Debug.Log("Other object is contained.");
			}

			//For each object that was removed, remove this objects id from it as a container and mark it not contained if there are no containers for it.
			foreach(string id in removeContains)
			{
				ObjectStateTracker otherTracker = trackerManager.GetObjectStateTrackerFromObjectID(id);
				otherTracker.isChanged = true;
				Debug.Log(otherTracker.gameObject.name + " is no longer contained in " + tracker.gameObject.name + ". change in " + otherTracker.gameObject.name);
				List<string> lastOtherContainer = new List<string>(otherTracker.state.containers);
				otherTracker.state.containers.Remove(tracker.state.id);
				otherTracker.state.changes.Add(new TrackedChange("containers", lastOtherContainer.ToString(), otherTracker.state.containers.ToString()));

				//-Debug.Log("Other object had a container removed.");

				if(otherTracker.state.containers.Count == 0)
				{
					bool lastOtherContained = otherTracker.state.contained;
					otherTracker.state.contained = false;
					otherTracker.state.changes.Add(new TrackedChange("contained", lastOtherContained.ToString(), otherTracker.state.contained.ToString()));

					//-Debug.Log("Other object is not contained.");
				}
			}
		}
		else
		{
			//This object was containing something but now isn't anymore since there are no colliders inside its bounds.
			if(tracker.state.containing)
			{
				tracker.isChanged = true;
				Debug.Log(tracker.gameObject.name + " used to contain something but now isnt anymore. change in " + tracker.gameObject.name);
				lastContaining = tracker.state.containing;
				lastContains.Clear();
				lastContains.AddRange(tracker.state.contains);

				foreach(string id in lastContains)
				{
					ObjectStateTracker otherTracker = trackerManager.GetObjectStateTrackerFromObjectID(id);
					otherTracker.isChanged = true;
					Debug.Log(otherTracker.gameObject.name + " is no longer contained in " + tracker.gameObject.name + ". change in " + otherTracker.gameObject.name);
					List<string> lastOtherContainer = new List<string>(otherTracker.state.containers);
					otherTracker.state.containers.Remove(tracker.state.id);
					otherTracker.state.changes.Add(new TrackedChange("containers", lastOtherContainer.ToString(), otherTracker.state.containers.ToString()));

					//-Debug.Log("Other object had a container removed.");

					if(otherTracker.state.containers.Count == 0)
					{
						bool lastOtherContained = otherTracker.state.contained;
						otherTracker.state.contained = false;
						otherTracker.state.changes.Add(new TrackedChange("contained", lastOtherContained.ToString(), otherTracker.state.contained.ToString()));

						//-Debug.Log("Other object is not contained.");
					}
				}

				tracker.state.containing = false;
				tracker.state.contains.Clear();

				//Manage tracked changes update.
				tracker.state.changes.Add(new TrackedChange("containing", lastContaining.ToString(), tracker.state.containing.ToString()));
				tracker.state.changes.Add(new TrackedChange("contains", lastContains.ToString(), tracker.state.contains.ToString()));

				//-Debug.Log("Object is not containing anything anymore.");
				//-Debug.Log("Object contains nothing anymore.");
			}
		}

		//TODO LOOK AT ALL CASES
	}

	public void ResetTracker()
	{
		//        Debug.Log("ContainmentTracker.ResetTracker()");
	}
}

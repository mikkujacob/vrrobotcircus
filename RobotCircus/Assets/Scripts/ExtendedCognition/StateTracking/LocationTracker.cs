﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Location tracker.
/// </summary>
public class LocationTracker : ITracker
{
    /// <summary>
    /// The object state tracker.
    /// </summary>
    private ObjectStateTracker objectStateTracker;

    /// <summary>
    /// The game object.
    /// </summary>
    private GameObject gameObject;

    /// <summary>
    /// The tracker update distance threshold.
    /// </summary>
    private float trackerUpdateDistanceThreshold;

    /// <summary>
    /// The object size to threshold ratio.
    /// </summary>
    public float objectSizeToThresholdRatio = 0.25f;

    /// <summary>
    /// The last location.
    /// </summary>
    private Vector3 lastLocation;

    /// <summary>
    /// Initializes a new instance of the <see cref="LocationTracker"/> class.
    /// </summary>
    /// <param name="tracker">Tracker.</param>
    public LocationTracker(ObjectStateTracker tracker)
    {
        this.objectStateTracker = tracker;
        this.gameObject = tracker.gameObject;
    }

    /// <summary>
    /// Starts the tracker.
    /// </summary>
    /// <param name="tracker">Tracker.</param>
    public void StartTracker()
    {
        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
        if(meshFilter == null)
        {
            throw new MissingComponentException();
        }
        Bounds meshBounds = meshFilter.mesh.bounds;
        float size = meshBounds.size.magnitude;
        trackerUpdateDistanceThreshold = size * objectSizeToThresholdRatio;
        Debug.Log("TrackerUpdateDistanceThreshold: " + trackerUpdateDistanceThreshold);
        lastLocation = gameObject.transform.position;
    }

    /// <summary>
    /// Updates the tracker.
    /// </summary>
    public void UpdateTracker()
    {
//        Debug.Log("LocationTracker.UpdateTracker()");

        if(Vector3.Distance(gameObject.transform.position, lastLocation) > trackerUpdateDistanceThreshold)
        {
            Vector3 position = gameObject.transform.position;
            //Debug.Log("Location change detected! Old Position: " + lastLocation.ToString() + ", New Position: " + gameObject.transform.position.ToString());
            objectStateTracker.state.location = new Vector3Definition(position.x, position.y, position.z);

			objectStateTracker.state.changes.Add(new TrackedChange("location", lastLocation.ToString(), position.ToString()));

            objectStateTracker.isChanged = true;
            lastLocation = position;
        }
	}

	public void ResetTracker()
	{
		lastLocation = gameObject.transform.position;
	}
}

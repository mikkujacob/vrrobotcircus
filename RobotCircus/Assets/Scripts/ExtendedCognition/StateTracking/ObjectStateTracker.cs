using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Object state tracker.
/// </summary>
public class ObjectStateTracker : MonoBehaviour
{
  /// <summary>
  /// keep track of current material
  /// </summary>
  private int materialCounter;

  /// <summary>
  /// The state.
  /// </summary>
  public ObjectStateDefinition state;

  /// <summary>
  /// The is changed.
  /// </summary>
  public bool isChanged;


  /// <summary>
  /// Gets the state JSO.
  /// </summary>
  /// <value>The state JSO.</value>
  public string stateJSON
  {
    get
    {
      return JsonUtility.ToJson(state);
    }
  }

  /// <summary>
  /// The osc bridge.
  /// </summary>
//  MImEGENIIOSCBridge oscBridge;

  /// <summary>
  /// The GestureSaver
  /// </summary>
//  GestureSaver gestureSaver;

  /// <summary>
  /// The active.
  /// </summary>
  private bool active;

  /// <summary>
  /// The trackers.
  /// </summary>
  private List<ITracker> trackers;

  /// <summary>
  /// the Global Tracker Manager
  /// </summary>
  public GlobalTrackerManager globalTrackerManager;


  /// <summary>
  /// Start this instance.
  /// </summary>
  public void Start()
  {
    materialCounter = 0;
    if (state == null)
    {
      state = new ObjectStateDefinition();
      initializeObjectStateDefinition();
    }


    isChanged = false;
    Debug.Log(stateJSON);

    GameObject globalObject = GameObject.FindGameObjectWithTag("MImEGlobalObject");
//    oscBridge = globalObject.GetComponent<MImEGENIIOSCBridge>();
//    gestureSaver = globalObject.GetComponent<GestureSaver>();

//    if(oscBridge == null)
//    {
//      throw new MissingComponentException();
//    }

    StartTrackers();

    Debug.Log("Trackers started");

    //Deactivate();
  }

  /// <summary>
  /// Initializes the object state definition.
  /// </summary>
  public void initializeObjectStateDefinition()
  {
    if (state == null)
    {
      state = new ObjectStateDefinition();
    }
    Debug.Log("Tracker.initializeObjectStateDefinition()");
//    state.globalTrackerManager = globalTrackerManager;
    //state.location = new Vector3Definition(gameObject.transform.position);
    //state.orientation = new Vector4Definition(gameObject.transform.rotation);
  }

  /// <summary>
  /// Starts the trackers.
  /// </summary>
  public void StartTrackers()
  {
    //Add all trackers to list
    trackers = new List<ITracker>();
    trackers.Add(new LocationTracker(this));
    trackers.Add(new OrientationTracker(this));
    trackers.Add(new TriggerContainmentTracker(this));
    trackers.Add(new SpatialTracker(this));


    //Start all trackers
    foreach(ITracker tracker in trackers)
    {
      tracker.StartTracker();
    }
  }

  /// <summary>
  /// Update this instance.
  /// </summary>
  public void Update()
  {
    //		Debug.Log("Tracker.Update()");

    UpdateTrackers();

    if(isChanged)
    {
      //Send message with object state definition
      sendStateJSON();
      state.changes = new List<TrackedChange>();
      isChanged = false;
      //changeMaterial();
    }
  }

  /// <summary>
  /// Sends the state JSON.
  /// </summary>
  public void sendStateJSON()
  {
    //oscBridge.SendMessage(stateJSON, "ObjectStateDefinition");
//    if (gestureSaver != null)
//    {
//      gestureSaver.WriteOSTJSON(stateJSON, state.id);
//    }

  }

  /// <summary>
  /// Updates the trackers.
  /// </summary>
  public void UpdateTrackers()
  {
    foreach(ITracker tracker in trackers)
    {

      tracker.UpdateTracker();



    }
  }

  /// <summary>
  /// Resets the trackers.
  /// </summary>
  public void ResetTrackers()
  {
    initializeObjectStateDefinition();
    foreach(ITracker tracker in trackers)
    {
      tracker.ResetTracker();
    }
  }

  /// <summary>
  /// Activate this instance.
  /// </summary>
  public void Activate()
  {
    active = true;
    enabled = active;
    ResetTrackers();
    Debug.Log("Tracker.Activate()");
  }

  /// <summary>
  /// Deactivate this instance.
  /// </summary>
  public void Deactivate()
  {
    active = false;
    enabled = active;
    Debug.Log("Tracker.Deactivate()");
  }

  public bool IsActive()
  {
    return active;
  }

  /// <summary>
  /// changes the material when a change is detected
  /// </summary>
  public void changeMaterial()
  {
    if (materialCounter == 0) {
      gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.magenta);
      materialCounter++;
    } else {
      gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
      materialCounter = 0;
    }

  }

  /// <summary>
  /// sets up the globalTrackerManager
  /// </summary>
  public void setGlobalTrackerManager(GlobalTrackerManager globalTrackerManager)
  {
    this.globalTrackerManager = globalTrackerManager;

  }

}

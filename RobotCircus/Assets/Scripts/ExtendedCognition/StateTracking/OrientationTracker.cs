﻿using UnityEngine;
using System.Collections;

public class OrientationTracker : ITracker
{
    /// <summary>
    /// The object state tracker.
    /// </summary>
    private ObjectStateTracker objectStateTracker;

    /// <summary>
    /// The game object.
    /// </summary>
    private GameObject gameObject;

    /// <summary>
    /// The tracker update rotation threshold.
    /// </summary>
    private float trackerUpdateRotationThreshold = 15;

    /// <summary>
    /// The last orientation.
    /// </summary>
    private Quaternion lastOrientation;

    public OrientationTracker(ObjectStateTracker tracker)
    {
        this.objectStateTracker = tracker;
        this.gameObject = tracker.gameObject;
    }

    /// <summary>
    /// Starts the tracker.
    /// </summary>
    /// <param name="tracker">Tracker.</param>
    public void StartTracker()
    {
        lastOrientation = gameObject.transform.rotation;
    }

    /// <summary>
    /// Updates the tracker.
    /// </summary>
    public void UpdateTracker()
    {
//        Debug.Log("Orientation.UpdateTracker()");

        if(Quaternion.Angle(lastOrientation, gameObject.transform.rotation) > trackerUpdateRotationThreshold)
        {
            Quaternion rotation = gameObject.transform.rotation;
			//Debug.Log("Orientation change detected! Old Orientation: " + lastOrientation.ToString() + ", New Orientation: " + gameObject.transform.rotation.ToString());

			objectStateTracker.state.changes.Add(new TrackedChange("orientation", lastOrientation.ToString(), rotation.ToString()));

            objectStateTracker.state.orientation = new Vector4Definition(rotation.x, rotation.y, rotation.z, rotation.w);
            objectStateTracker.isChanged = true;
            lastOrientation = rotation;
        }
	}

	public void ResetTracker()
	{
		lastOrientation = gameObject.transform.rotation;
	}
}

﻿Shader "Custom/insides" {
          Properties {
             _MainTex ("Base (RGB)", 2D) = "white" {}
             _BumpMap ("Normal map", 2D) = "bump" {}
         }
          SubShader {
            
            Tags { "RenderType" = "Opaque" }
            
            Cull Front
            
            CGPROGRAM
            
            #pragma surface surf Lambert vertex:vert
            sampler2D _MainTex;
            sampler2D _BumpMap;
     
             struct Input {
                 float2 uv_MainTex;
                 float2 uv_BumpMap;
                 float4 color : COLOR;
             };
            
            
            void vert(inout appdata_full v)
            {
                v.normal.xyz = v.normal * -1;
            }
            
            void surf (Input IN, inout SurfaceOutput o) {
                 o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
                 o.Alpha = 1;
                 o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
            }
            
            ENDCG
            
          }
          
          Fallback "Diffuse"

}
